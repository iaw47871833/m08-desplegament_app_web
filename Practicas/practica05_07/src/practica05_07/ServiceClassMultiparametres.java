package practica05_07;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component		// Java Annotation perquè es crei un bean d'aquesta classe automàticament.
public class ServiceClassMultiparametres {
	private final BeanFactory factory;
	private List<Personal> personalMultiparametres;

	
	
	/*
	public ServiceClassMultiparametres(BeanFactory factory, List<Tripulant> protoTypeBeanMultiparametres) {
		this.factory = factory;
		this.tripulantMultiparametres = protoTypeBeanMultiparametres;
	}
	*/

	@Autowired
	public ServiceClassMultiparametres(final BeanFactory f) {
		this.factory = f;
	}

	
	public void demoMethod(List<ObjIniProtoTypeBeanMultiparametres> someArrayList) {
		// Various versions of getBean() method return an instance of the specified bean.
		
		// Recorre tota la llista 'someArrayList' a saco creant un flux amd les dades que conté (stream()) i 
		// per a cada element (param) aplica la funció que li passem dins del map() de manera que ens retorna
		// un flux amb tots els resultats (un flux amb els objectes de tipus Tripulant creats amb
		// 'factory.getBean(Tripulant.class, param)' i inicialitzats segons el
		// contructor Tripulant.Tripulant(ObjIniProtoTypeBeanMultiparametres objIniProtoTypeBeanMultiparametres){}).
		//
		// Converteix el flux resultant en una llista gràcies al collect(Collectors.toList()). 
		// El collect() és el que canviarà el flux de dades en un altre cosa que serà una llista gràcies al paràmetre 'Collectors.toList()'.
		
		this.personalMultiparametres = someArrayList.stream().map(param -> factory.getBean(Personal.class, param))
				.collect(Collectors.toList());
		
	}
	

	public List<Personal> getPersonalMultiparametres() {
		return personalMultiparametres;
	}


}
