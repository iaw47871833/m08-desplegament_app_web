package practica05_07practica05_07;

import java.time.LocalDate;

public class Personal {
	private String dni;
	private String nom;
	private String cognom;
	private String email;
	private String login;
	private String contrasenya;
	private int departament;
	private String conneixements;
	private String ciutatNaixement;
	private String idiomes;
	private int edat;
	private String telefon;
	private LocalDate dataCreacio;
	
	/**
	 * @param dni
	 * @param nom
	 * @param cognom
	 * @param email
	 * @param login
	 * @param contrasenya
	 * @param departament
	 * @param conneixements
	 * @param ciutatNaixement
	 * @param idiomes
	 * @param edat
	 * @param telefon
	 * @param dataCreacio
	 */
	public Personal(String dni, String nom, String cognom, String email, String login, String contrasenya,
			int departament, String conneixements, String ciutatNaixement, String idiomes, int edat, String telefon,
			LocalDate dataCreacio) {
		this.dni = dni;
		this.nom = nom;
		this.cognom = cognom;
		this.email = email;
		this.login = login;
		this.contrasenya = contrasenya;
		this.departament = departament;
		this.conneixements = conneixements;
		this.ciutatNaixement = ciutatNaixement;
		this.idiomes = idiomes;
		this.edat = edat;
		this.telefon = telefon;
		this.dataCreacio = dataCreacio;
	}
	
	/**
	 * @return the dni
	 */
	public String getDni() {
		return dni;
	}
	/**
	 * @param dni the dni to set
	 */
	public void setDni(String dni) {
		this.dni = dni;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the cognom
	 */
	public String getCognom() {
		return cognom;
	}
	/**
	 * @param cognom the cognom to set
	 */
	public void setCognom(String cognom) {
		this.cognom = cognom;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}
	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}
	/**
	 * @return the contrasenya
	 */
	public String getContrasenya() {
		return contrasenya;
	}
	/**
	 * @param contrasenya the contrasenya to set
	 */
	public void setContrasenya(String contrasenya) {
		this.contrasenya = contrasenya;
	}
	/**
	 * @return the departament
	 */
	public int getDepartament() {
		return departament;
	}
	/**
	 * @param departament the departament to set
	 */
	public void setDepartament(int departament) {
		this.departament = departament;
	}
	/**
	 * @return the conneixements
	 */
	public String getConneixements() {
		return conneixements;
	}
	/**
	 * @param conneixements the conneixements to set
	 */
	public void setConneixements(String conneixements) {
		this.conneixements = conneixements;
	}
	/**
	 * @return the ciutatNaixement
	 */
	public String getCiutatNaixement() {
		return ciutatNaixement;
	}
	/**
	 * @param ciutatNaixement the ciutatNaixement to set
	 */
	public void setCiutatNaixement(String ciutatNaixement) {
		this.ciutatNaixement = ciutatNaixement;
	}

	/**
	 * @return the idiomes
	 */
	public String getIdiomes() {
		return idiomes;
	}

	/**
	 * @param idiomes the idiomes to set
	 */
	public void setIdiomes(String idiomes) {
		this.idiomes = idiomes;
	}

	/**
	 * @return the edat
	 */
	public int getEdat() {
		return edat;
	}

	/**
	 * @param edat the edat to set
	 */
	public void setEdat(int edat) {
		this.edat = edat;
	}

	/**
	 * @return the telefon
	 */
	public String getTelefon() {
		return telefon;
	}

	/**
	 * @param telefon the telefon to set
	 */
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	/**
	 * @return the dataCreacio
	 */
	public LocalDate getDataCreacio() {
		return dataCreacio;
	}

	/**
	 * @param dataCreacio the dataCreacio to set
	 */
	public void setDataCreacio(LocalDate dataCreacio) {
		this.dataCreacio = dataCreacio;
	}
}
