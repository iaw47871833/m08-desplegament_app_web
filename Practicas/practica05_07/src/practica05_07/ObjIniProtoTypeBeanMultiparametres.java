package practica05_07;

import java.time.LocalDate;

public class ObjIniProtoTypeBeanMultiparametres {
	private String dni;
	private String nom;
	private LocalDate dataCreacio;
	private int departament;

	/**
	 * @param dni
	 * @param nom
	 * @param dataCreacio
	 * @param departament
	 */
	public ObjIniProtoTypeBeanMultiparametres(String dni, String nom, LocalDate dataCreacio, int departament) {
		super();
		this.dni = dni;
		this.nom = nom;
		this.dataCreacio = dataCreacio;
		this.departament = departament;
	}

	/**
	 * @return the dni
	 */
	public String getDni() {
		return dni;
	}

	/**
	 * @param dni the dni to set
	 */
	public void setDni(String dni) {
		this.dni = dni;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the dataCreacio
	 */
	public LocalDate getDataCreacio() {
		return dataCreacio;
	}

	/**
	 * @param dataCreacio the dataCreacio to set
	 */
	public void setDataCreacio(LocalDate dataCreacio) {
		this.dataCreacio = dataCreacio;
	}

	/**
	 * @return the departament
	 */
	public int getDepartament() {
		return departament;
	}

	/**
	 * @param departament the departament to set
	 */
	public void setDepartament(int departament) {
		this.departament = departament;
	}

	
	
}
