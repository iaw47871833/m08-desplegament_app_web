package practica05_07;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Controlador {
	List<Personal> llistaProtoTypeBeanMultiparametresPersonal;
	List<Departament> llistaProtoTypeBeanMultiparametresDepartaments_v2;
	

	@RequestMapping("/")
	public String inici() {
		llistaProtoTypeBeanMultiparametresPersonal = usAplicacio.inicialitzarPersonals();
		llistaProtoTypeBeanMultiparametresDepartaments_v2 = usAplicacio.inicialitzarDepartaments_v2();
		
		System.out.println();
		System.out.println("------llistaProtoTypeBeanMultiparametresPersonal------");
		
		int j = 1;
		for(Personal personalTmp : llistaProtoTypeBeanMultiparametresPersonal) {
			System.out.println(j + ": PERSONAL = " + personalTmp + 
					"\n      personalTmp.getDni() = " + personalTmp.getDni() +
					"\n      personalTmp.getNom() = " + personalTmp.getNom() +
					"\n      personalTmp.getDepartament() = " + personalTmp.getDepartament() +
					"\n      personalTmp.getDataCreacio() = " + personalTmp.getDataCreacio());
			j++;
		}
		
		System.out.println();
		System.out.println("------llistaProtoTypeBeanMultiparametresDepartaments_v2------");
		
		j = 1;
		for(Departament departamentTmp_v2 : llistaProtoTypeBeanMultiparametresDepartaments_v2) {
			System.out.println(j + ": DEPARTAMENT_v2" + 
					"\n      departamentTmp_v2.getId() = " + departamentTmp_v2.getId() +
					"\n      departamentTmp_v2.getNom() = " + departamentTmp_v2.getNom() + 
					"\n      departamentTmp_v2.getNom() = " + departamentTmp_v2.getEmail());
			j++;
		}
		
		return "inici";
	}
}
