package practica05_07;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class usAplicacio {
	static public List<Personal> inicialitzarPersonals() {
		AnnotationConfigApplicationContext contexteAmbClasseConfig = new AnnotationConfigApplicationContext(aplicacioConfig.class);
		
		// Exemple d'inserció obj's <> inicialitzats amb 3 paràmetres:
		// https://stackoverflow.com/questions/55096855/how-to-ask-for-prototype-bean-in-spring-service-class-without-applicationcontext
		// Al final de todo, la respuesta del 11 de marzo del 2019 de Andy Brown.
		
		ServiceClassMultiparametres serviceClassMultiparametres = contexteAmbClasseConfig.getBean("serviceClassMultiparametres", ServiceClassMultiparametres.class);
		
		ArrayList<ObjIniProtoTypeBeanMultiparametres> llistaObjIniProtoTypeBeanMultiparametres = new ArrayList();
		ObjIniProtoTypeBeanMultiparametres personal_1 = new ObjIniProtoTypeBeanMultiparametres("47281933T", "Alex", LocalDate.now(), 1);
		ObjIniProtoTypeBeanMultiparametres personal_2 = new ObjIniProtoTypeBeanMultiparametres("48372811M", "Victor", LocalDate.now(), 2);
		ObjIniProtoTypeBeanMultiparametres personal_3 = new ObjIniProtoTypeBeanMultiparametres("46829122R", "Tomas", LocalDate.now(), 1);
		llistaObjIniProtoTypeBeanMultiparametres.add(personal_1);
		llistaObjIniProtoTypeBeanMultiparametres.add(personal_2);
		llistaObjIniProtoTypeBeanMultiparametres.add(personal_3);
		
		serviceClassMultiparametres.demoMethod(llistaObjIniProtoTypeBeanMultiparametres);
		
		contexteAmbClasseConfig.close();
		
		return serviceClassMultiparametres.getPersonalMultiparametres();
	}
	
	

	
	static public List<Departament> inicialitzarDepartaments_v2() {
		AnnotationConfigApplicationContext contexteAmbClasseConfig = new AnnotationConfigApplicationContext(aplicacioConfig.class);
		
		// Exemple d'inserció obj's <> inicialitzats amb 3 paràmetres (versió 2):
		
		ServiceClassMultiparametres2_v2 serviceClassMultiparametres2_v2 = contexteAmbClasseConfig.getBean("serviceClassMultiparametres2_v2", ServiceClassMultiparametres2_v2.class);
		
		List<Integer> llistaIdsDepartament = Arrays.asList(1, 2, 3, 4);
		List<String> llistaNomsDepartament = Arrays.asList("departament_1", "departament_2", "departament_3", "departament_4");
		List<String> llistaEmailsDepartament = Arrays.asList("departament_1@gmail.cat", "departament_2@gmail.cat", "departament_3@gmail.cat", "departament_4@gmail.cat");
		
		serviceClassMultiparametres2_v2.demoMethod_v2(llistaIdsDepartament, llistaNomsDepartament, llistaEmailsDepartament);
		
		contexteAmbClasseConfig.close();
		
		return serviceClassMultiparametres2_v2.getLlistaDepartamentsMultiparametres();
	}
}
