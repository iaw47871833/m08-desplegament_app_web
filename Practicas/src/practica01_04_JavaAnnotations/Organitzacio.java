package practica01_04_JavaAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//@Component("organitzacio")
public class Organitzacio {
	// Atributs
	private String nom;
	private InformeInterface informe;
	
	/**
	 * @param nom
	 * @param informe
	 */
	@Autowired
	public Organitzacio(String nom, InformeInterface informe) {
		this.nom = nom;
		this.informe = informe;
	}

	@Override
	public String toString() {
		return "Organitzacio [nom=" + nom + ", informe=" + informe.getInforme() + "]";
	}
	
	
	
}
