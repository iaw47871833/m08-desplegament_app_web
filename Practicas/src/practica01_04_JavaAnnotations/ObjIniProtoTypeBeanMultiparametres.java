package practica01_04_JavaAnnotations;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class ObjIniProtoTypeBeanMultiparametres {
	// Atributs
	private String dni;
	private String nom;
	private String cognom;
	private String email;
	private int numDepartament;
	/**
	 * @param dni
	 * @param nom
	 * @param cognom
	 * @param email
	 * @param numDepartament
	 */
	public ObjIniProtoTypeBeanMultiparametres(String dni, String nom, String cognom, String email, int numDepartament) {
		this.dni = dni;
		this.nom = nom;
		this.cognom = cognom;
		this.email = email;
		this.numDepartament = numDepartament;
	}
	/**
	 * @return the dni
	 */
	public String getDni() {
		return dni;
	}
	/**
	 * @param dni the dni to set
	 */
	public void setDni(String dni) {
		this.dni = dni;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the cognom
	 */
	public String getCognom() {
		return cognom;
	}
	/**
	 * @param cognom the cognom to set
	 */
	public void setCognom(String cognom) {
		this.cognom = cognom;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the numDepartament
	 */
	public int getNumDepartament() {
		return numDepartament;
	}
	/**
	 * @param numDepartament the numDepartament to set
	 */
	public void setNumDepartament(int numDepartament) {
		this.numDepartament = numDepartament;
	}
	
	
}
