package practica01_04_JavaAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PreDestroy;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("departamentTipus2")
@Scope("singleton")

//Indiquem la ruta cap a l'arxiu extern de propietats
@PropertySource("classpath:practica01-04Departaments.propietats")  
public class Departament {
	// Atributs
	// Injectem el valor de la propietat id
	@Value("${id}")
	private int id;
	// Injectem el valor de la propietat nom
	@Value("${nom}")
	private String nom;
	// Injectem el valor de la propietat email
	@Value("${email}")
	private String email;
	private Organitzacio organitzacio;
	
	@Autowired
	@Qualifier("dep_b_per_1")
	private personalInterface capDeDepartament;
	private ArrayList<Dep_B_per_2> llistaPersonal;
	private InformeDepartamentInterface informeDepartament;
	private final BeanFactory factory;

	// Indiquem amb Autowired que faci servir aquest constructor per a generar beans
	@Autowired
	public Departament(InformeDepartamentInterface inf, Dep_B_per_1 personal1, BeanFactory factory, ArrayList<Dep_B_per_2> llistaPersonal) {
		this.informeDepartament = inf;
		this.capDeDepartament = personal1;
		this.factory = factory;
		this.llistaPersonal = llistaPersonal;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	

	/**
	 * @return the organitzacio
	 */
	public Organitzacio getOrganitzacio() {
		return organitzacio;
	}

	/**
	 * @param organitzacio the organitzacio to set
	 */
	@Autowired
	public void setOrganitzacio(Organitzacio organitzacio) {
		this.organitzacio = organitzacio;
	}

	/**
	 * @return the capDeDepartament
	 */
	public personalInterface getCapDeDepartament() {
		return capDeDepartament;
	}

	/**
	 * @param capDeDepartament the capDeDepartament to set
	 */
	public void setCapDeDepartament(personalInterface capDeDepartament) {
		this.capDeDepartament = capDeDepartament;
	}

	

	/**
	 * @return the llistaPersonal
	 */
	public ArrayList<Dep_B_per_2> getLlistaPersonal() {
		return llistaPersonal;
	}

	/**
	 * @param llistaPersonal the llistaPersonal to set
	 */
	public void setLlistaPersonal(List<ObjIniProtoTypeBeanMultiparametres> someArrayList) {
		this.llistaPersonal = (ArrayList<Dep_B_per_2>) someArrayList.stream()
				.map(param -> factory.getBean(Dep_B_per_2.class, param)).collect(Collectors.toList());
	}


	/**
	 * @return the informeDepartament
	 */
	public InformeDepartamentInterface getInformeDepartament() {
		return informeDepartament;
	}

	/**
	 * @param informeDepartament the informeDepartament to set
	 */
	public void setInformeDepartament(InformeDepartamentInterface informeDepartament) {
		this.informeDepartament = informeDepartament;
	}

	@Override
	public String toString() {
		return " id = " + id + "\n nom = " + nom + "\n email = " + email + "\n organitzacio = " + organitzacio
				+ "\n capDeDepartament = " + capDeDepartament + "\n llistaPersonal = " + llistaPersonal
				+ "\n informeDepartament = " + informeDepartament.getInformeDepartament() + "]";
	}

}
