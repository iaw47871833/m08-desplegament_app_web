package practica01_04_JavaAnnotations;

import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

@Component("dep_b_per_1")
public class Dep_B_per_1 implements personalInterface {

	// Mostrar un missatge abans de destruir
	@PreDestroy
	public void finalitzador() {
		System.out.println("maas mataooo");
	}

	@Override
	public String mostrarMissatge() {
		return "Sóc el personal 1 del departament B";
	}

}
