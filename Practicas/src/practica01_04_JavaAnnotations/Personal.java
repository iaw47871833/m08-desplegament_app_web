package practica01_04_JavaAnnotations;

public class Personal {
	// Atributs
	private String dni;
	private String nom;
	private String cognom;
	private String email;
	private int numDepartament; // ID del departament al qual està assignat
	
	/**
	 * @return the dni
	 */
	public String getDni() {
		return dni;
	}
	/**
	 * @param dni the dni to set
	 */
	public void setDni(String dni) {
		this.dni = dni;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the cognom
	 */
	public String getCognom() {
		return cognom;
	}
	/**
	 * @param cognom the cognom to set
	 */
	public void setCognom(String cognom) {
		this.cognom = cognom;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the numDepartament
	 */
	public int getNumDepartament() {
		return numDepartament;
	}

	/**
	 * @param numDepartament the numDepartament to set
	 */
	public void setNumDepartament(int numDepartament) {
		this.numDepartament = numDepartament;
	}


	@Override
	public String toString() {
		return "dni = " + dni + ", nom = " + nom + ", cognom = " + cognom + ", email = " + email + ", numDepartament = "
				+ numDepartament;
	}
}
