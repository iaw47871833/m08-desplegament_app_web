package practica01_04_JavaAnnotations;

public class Dep_B_per_2 implements personalInterface {

	private String dni;
	private String nom;
	private String cognom;
	private String email;
	private int numDepartament;

	public Dep_B_per_2(ObjIniProtoTypeBeanMultiparametres obj) {
		this.dni = obj.getDni();
		this.nom = obj.getNom();
		this.cognom = obj.getCognom();
		this.email = obj.getEmail();
		this.numDepartament = obj.getNumDepartament();
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCognom() {
		return cognom;
	}

	public void setCognom(String cognom) {
		this.cognom = cognom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getNumDepartament() {
		return numDepartament;
	}

	public void setNumDepartament(int numDepartament) {
		this.numDepartament = numDepartament;
	}

	@Override
	public String mostrarMissatge() {
		return "Sóc el personal 2 del departament B";
	}

}
