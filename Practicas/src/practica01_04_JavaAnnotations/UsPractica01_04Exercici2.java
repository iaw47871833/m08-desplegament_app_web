package practica01_04_JavaAnnotations;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsPractica01_04Exercici2 {

	public static void main(String[] args) {
		
		System.out.println("ABANS DE CARREGAR EL CONTEXT");

		// Creació del contexte
		AnnotationConfigApplicationContext contexte = new AnnotationConfigApplicationContext(practica01_04Config.class);

		System.out.println("DESPRÉS DE CARREGAR EL CONTEXT i ABANS DE CARREGAR EL BEAN");

		System.out.println("----Exercici Practica 2----");
		
		Dep_B_per_1 capDepartament = contexte.getBean("dep_b_per_1", Dep_B_per_1.class);
		//Organitzacio organitzacio = contexte.getBean("organitzacio", Organitzacio.class);
		// Creacio del bean departament
		Departament depB = contexte.getBean("departamentTipus2", Departament.class);
		// Creació de la llista de personal
		List<ObjIniProtoTypeBeanMultiparametres> llistaObjIniProtoTypeBeanMultiparametres = new ArrayList();
		// Afegim el personal
		ObjIniProtoTypeBeanMultiparametres per1 = new ObjIniProtoTypeBeanMultiparametres("48291933L", "Alex", "Francisco", "alex@alex.es", 2);
		ObjIniProtoTypeBeanMultiparametres per2 = new ObjIniProtoTypeBeanMultiparametres("40392921P", "Cristina", "Marquez", "cris@cris.es", 2);
		ObjIniProtoTypeBeanMultiparametres per3 = new ObjIniProtoTypeBeanMultiparametres("42382193T", "Ronald", "McDonald", "ronald@ronald.es", 2);
		llistaObjIniProtoTypeBeanMultiparametres.add(per1);
		llistaObjIniProtoTypeBeanMultiparametres.add(per2);
		llistaObjIniProtoTypeBeanMultiparametres.add(per3);
		
		// Fiquem la llista
		//depB.setLlistaPersonal(llistaObjIniProtoTypeBeanMultiparametres);
		
		// Mostrem la seva informacio
		System.out.println("Departament id: " + depB.getId());
		System.out.println("Departament nom: " + depB.getNom());
		System.out.println("Departament email: " + depB.getEmail());
		System.out.println("Departament organitzacio: " + depB.getOrganitzacio());
		System.out.println("Departament cap departament: " + depB.getCapDeDepartament().mostrarMissatge());
		
		
		// Tanquem el contexte
		contexte.close();
	
	}

}
