package practica01_04_JavaAnnotations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Configuration

@ComponentScan("practica01_04_JavaAnnotations")

public class practica01_04Config {
	@Bean
	public personalInterface empleat1() {
		return new Dep_B_per_1();
	}

	// Bean per crear una organització
	@Bean
	public Organitzacio getOrganitzacio() {
		return new Organitzacio("organitzacio", informe());
	}
	// Bean per crear una informe
	@Bean Informe informe() {
		return new Informe();
	}
}
