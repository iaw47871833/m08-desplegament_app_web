package practica01_04_JavaAnnotations;

import org.springframework.stereotype.Component;

@Component("informeDepartament")

public class InformeDepartament implements InformeDepartamentInterface {

	@Override
	public String getInformeDepartament() {
		return "Informe de InformeDepartament.java";
	}

}
