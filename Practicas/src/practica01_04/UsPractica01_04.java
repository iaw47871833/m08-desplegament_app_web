package practica01_04;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsPractica01_04 {

	public static void main(String[] args) {
		// Creem el contexte carregant el fitxer xml
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext_Practica01-04.xml");		
		
		// Creem una organitzacio
		Organitzacio o = contexte.getBean("organitzacio", Organitzacio.class);
		// Mostrem el seu informe
		System.out.println(o.toString());
		
		// Creem el primer personal de departament
		Personal p1 = contexte.getBean("dep-A-per-1", Personal.class);
		System.out.println("Personal 1:");
		System.out.println(p1.toString());
		
		// Creem el segon personal de departament
		Personal p2 = contexte.getBean("dep-A-per-2", Personal.class);
		System.out.println("Personal 2:");
		System.out.println(p2.toString());
		
		// Creem el cap de departament
		Personal capDepartament = contexte.getBean("capDeDepartament", Personal.class);
		System.out.println("Cap de departament:");
		System.out.println(capDepartament.toString());
		
		// Creem el personal
		Departament d = contexte.getBean("departamentTipus1", Departament.class);
		// Mostrem l'informe del departament
		System.out.println("Departament:");
		System.out.println(d.toString());
		
		// Tanquem el contexte
		contexte.close();
	}

}
