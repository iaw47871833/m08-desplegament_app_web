package practica01_04;

public class Organitzacio {
	// Atributs
	private String nom;
	private InformeInterface informe;
	/**
	 * @param nom
	 * @param informe
	 */
	public Organitzacio(InformeInterface informe) {
		this.informe = informe;
	}
	@Override
	public String toString() {
		return "Organitzacio [nom=" + nom + ", informe=" + informe.getInforme() + "]";
	}
	
	
	
}
