package practica01_04;

import java.util.ArrayList;

public class Departament {
	// Atributs
	private int id;
	private String nom;
	private String email;
	private Organitzacio organitzacio;
	private Personal capDeDepartament;
	private ArrayList<Personal> llistaPersonal;
	private InformeDepartamentInterface informeDepartament;
	
	public Departament(InformeDepartamentInterface inf) {
		this.informeDepartament = inf;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the organitzacio
	 */
	public Organitzacio getOrganitzacio() {
		return organitzacio;
	}

	/**
	 * @param organitzacio the organitzacio to set
	 */
	public void setOrganitzacio(Organitzacio organitzacio) {
		this.organitzacio = organitzacio;
	}

	/**
	 * @return the capDeDepartament
	 */
	public Personal getCapDeDepartament() {
		return capDeDepartament;
	}

	/**
	 * @param capDeDepartament the capDeDepartament to set
	 */
	public void setCapDeDepartament(Personal capDeDepartament) {
		this.capDeDepartament = capDeDepartament;
	}

	/**
	 * @return the llistaPersonal
	 */
	public ArrayList<Personal> getLlistaPersonal() {
		return llistaPersonal;
	}

	/**
	 * @param llistaPersonal the llistaPersonal to set
	 */
	public void setLlistaPersonal(ArrayList<Personal> llistaPersonal) {
		this.llistaPersonal = llistaPersonal;
	}

	/**
	 * @return the informeDepartament
	 */
	public InformeDepartamentInterface getInformeDepartament() {
		return informeDepartament;
	}

	/**
	 * @param informeDepartament the informeDepartament to set
	 */
	public void setInformeDepartament(InformeDepartamentInterface informeDepartament) {
		this.informeDepartament = informeDepartament;
	}

	@Override
	public String toString() {
		return " id = " + id + "\n nom = " + nom + "\n email = " + email + "\n organitzacio = " + organitzacio
				+ "\n capDeDepartament = " + capDeDepartament + "\n llistaPersonal = " + llistaPersonal
				+ "\n informeDepartament = " + informeDepartament.getInformeDepartament() + "]";
	}
	
	
}
