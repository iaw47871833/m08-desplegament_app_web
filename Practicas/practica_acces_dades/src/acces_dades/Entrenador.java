package acces_dades;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="entrenador")
public class Entrenador {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name="nom")
	private String nom;
	@Column(name="cognom")
	private String cognom;
	@ManyToOne(cascade= {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name = "equipId")
	private Equip equip;
	
	/**
	 * 
	 */
	public Entrenador() {
		
	}
	/**
	 * @param nom
	 * @param cognom
	 */
	public Entrenador(String nom, String cognom) {
		this.nom = nom;
		this.cognom = cognom;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the cognom
	 */
	public String getCognom() {
		return cognom;
	}
	/**
	 * @param cognom the cognom to set
	 */
	public void setCognom(String cognom) {
		this.cognom = cognom;
	}
	/**
	 * @return the equip
	 */
	public Equip getEquip() {
		return equip;
	}
	/**
	 * @param equip the equip to set
	 */
	public void setEquip(Equip equip) {
		this.equip = equip;
	}
	@Override
	public String toString() {
		return "Entrenador [id=" + id + ", nom=" + nom + ", cognom=" + cognom + "]";
	}
}
