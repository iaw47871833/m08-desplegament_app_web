package acces_dades;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ObtenirEntrenadorsVsEquips {

	public static void main(String[] args) {
		System.out.println("Creem l'objecte SessionFactory");
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Equip.class)
				.addAnnotatedClass(EquipDadesExtres.class)
				.addAnnotatedClass(Entrenador.class)
				.buildSessionFactory();
		System.out.println("Creem l'objecte Session");
		Session elMeuSession = elMeuFactory.openSession();
		
		try {
			System.out.println("Iniciem la transacció");
			elMeuSession.beginTransaction();
			
			Equip equipLlegitDeLaBD = elMeuSession.get(Equip.class, 4);
			
			if (equipLlegitDeLaBD != null) {
				System.out.println("Equip: " + equipLlegitDeLaBD);
				System.out.println("Entrenadors assignats: " + equipLlegitDeLaBD.getLlistaEntrenadors());
			} else {
				System.out.println("NO S'HA TROBAT CAP EQUIP.");
			}
			
			elMeuSession.getTransaction().commit();
			
			System.out.println("Acabem la transacció. Registres SELECT en la BD");
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}
	}

}
