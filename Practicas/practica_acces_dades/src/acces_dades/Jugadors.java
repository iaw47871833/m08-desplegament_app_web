package acces_dades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="jugadors")
public class Jugadors {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name="nom")
	private String nom;
	@Column(name="cognom")
	private String cognom;
	@Column(name="posicio")
	private String posicio;
	@Column(name="equip")
	private String equip;
	
	/**
	 * Constructor sense parametres
	 */
	public Jugadors() {
	}

	/**
	 * @param id
	 * @param nom
	 * @param cognom
	 * @param posicio
	 * @param equip
	 */
	public Jugadors(String nom, String cognom, String posicio, String equip) {
		this.nom = nom;
		this.cognom = cognom;
		this.posicio = posicio;
		this.equip = equip;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the cognom
	 */
	public String getCognom() {
		return cognom;
	}

	/**
	 * @param cognom the cognom to set
	 */
	public void setCognom(String cognom) {
		this.cognom = cognom;
	}

	/**
	 * @return the posicio
	 */
	public String getPosicio() {
		return posicio;
	}

	/**
	 * @param posicio the posicio to set
	 */
	public void setPosicio(String posicio) {
		this.posicio = posicio;
	}

	/**
	 * @return the equip
	 */
	public String getEquip() {
		return equip;
	}

	/**
	 * @param equip the equip to set
	 */
	public void setEquip(String equip) {
		this.equip = equip;
	}

	@Override
	public String toString() {
		return "Jugadors [id=" + id + ", nom=" + nom + ", cognom=" + cognom + ", posicio=" + posicio + ", equip="
				+ equip + "]";
	}
	
	
}
