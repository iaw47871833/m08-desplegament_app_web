package acces_dades;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HQLDeleteJugadors {

	public static void main(String[] args) {
		System.out.println("Creem l'objecte SessionFactory");
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Jugadors.class).buildSessionFactory();
		System.out.println("Creem l'objecte Session");
		Session elMeuSession = elMeuFactory.openSession();
	
		try {
			System.out.println("Iniciem la transacció");
			elMeuSession.beginTransaction();
			
			System.out.println();
			
			String sentenciaHQL = "DELETE Jugadors WHERE nom = 'Shaquille'";
			System.out.println("1: Llançem la sentencia '" + sentenciaHQL + "'.");
			elMeuSession.createQuery(sentenciaHQL).executeUpdate();
			
			
			elMeuSession.getTransaction().commit();
			
			System.out.println("Hem acabat de llegit un registre de la BD.");
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}

	}

}
