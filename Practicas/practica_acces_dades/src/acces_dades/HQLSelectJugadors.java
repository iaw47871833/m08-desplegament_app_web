package acces_dades;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HQLSelectJugadors {

	public static void main(String[] args) {
		System.out.println("Creem l'objecte SessionFactory");
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Jugadors.class).buildSessionFactory();
		
		System.out.println("Creem l'objecte Session");
		Session elMeuSession = elMeuFactory.openSession();
	
		try {
			System.out.println("Iniciem la transacció");
			elMeuSession.beginTransaction();
			
//			String sentenciaHQL = "FROM Jugadors";
			String sentenciaHQL = "FROM Jugadors WHERE equip = 'Brooklyn Nets'";
			List<Jugadors> llistaJugadors = elMeuSession.createQuery(sentenciaHQL).getResultList();
			
			for (Jugadors jugadorTmp: llistaJugadors) {
				System.out.println(jugadorTmp);
			}
		
			
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}

	}

}
