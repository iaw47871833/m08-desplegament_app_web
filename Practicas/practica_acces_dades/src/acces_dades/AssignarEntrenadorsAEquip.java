package acces_dades;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class AssignarEntrenadorsAEquip {

	public static void main(String[] args) {
		System.out.println("Creem l'objecte SessionFactory");
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Equip.class)
				.addAnnotatedClass(EquipDadesExtres.class)
				.addAnnotatedClass(Entrenador.class)
				.buildSessionFactory();
		System.out.println("Creem l'objecte Session");
		Session elMeuSession = elMeuFactory.openSession();
		
		try {
			System.out.println("Iniciem la transacció");
			elMeuSession.beginTransaction();
			
			Equip equipLlegitDeLaBD = elMeuSession.get(Equip.class, 4);
			
			// Creem els entrenadors
			Entrenador entrenador_1 = new Entrenador("Frank", "Vogel");
			Entrenador entrenador_2 = new Entrenador("Terry", "Sttots");
			Entrenador entrenador_3 = new Entrenador("Greg", "Popovich");
			
			// Guardem els entrenadors
			elMeuSession.save(entrenador_1);
			elMeuSession.save(entrenador_2);
			elMeuSession.save(entrenador_3);
			
			elMeuSession.getTransaction().commit();
			
			System.out.println("Acabem la transacció. Registres INSERTAT'S en la BD");
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}
	}

}
