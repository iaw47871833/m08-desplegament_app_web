package acces_dades;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class EsborrarEquipUnAUnBidireccional {

	public static void main(String[] args) {
		System.out.println("Creem l'objecte SessionFactory");
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Equip.class)
				.addAnnotatedClass(EquipDadesExtres.class)
				.buildSessionFactory();
		System.out.println("Creem l'objecte Session");
		Session elMeuSession = elMeuFactory.openSession();
		
		try {
			System.out.println("Iniciem la transacció");
			elMeuSession.beginTransaction();
			
			System.out.println("DELETE Equip --> EquipDadesExtres:");
			Equip equipLlegitDeLaBD = elMeuSession.get(Equip.class, 3);
			
			if (equipLlegitDeLaBD != null) {
				System.out.println("Registre llegit de la BD (equip): " + equipLlegitDeLaBD.getNom() + "(id: " + equipLlegitDeLaBD.getId());
				System.out.println("Registre llegit de la BD (equip_dades_extres): " + equipLlegitDeLaBD.getEquipDadesExtres());
				
				System.out.println("Esborrem el equipLlegitDeLaBD i en cascada l'objecte EquipDadesEXtres associat");
				elMeuSession.delete(equipLlegitDeLaBD);
			} else {
				System.out.println("No s'ha trobat cap equip amb id = 3");
			}
			
			elMeuSession.getTransaction().commit();
			
			if (equipLlegitDeLaBD != null) {
				System.out.println("Acabem la transaccio. Registre DELETE en la BD");
			} else {
				System.out.println("Acabem la transaccio. No s'ha fet cap DELETE en la BD");
			}
			
			System.out.println("");
			
			System.out.println("Iniciem la 2a transacció.");
			
			elMeuSession.beginTransaction();
			
			System.out.println("DELETE EquipDadesEXtres --> Equip:");
			EquipDadesExtres equipDadesExtresLlegitDeLaBD = elMeuSession.get(EquipDadesExtres.class, 2);
			
			if (equipDadesExtresLlegitDeLaBD != null) {
				System.out.println("Registre llegit de la BD (equip_dades_extres): " + equipDadesExtresLlegitDeLaBD.toString());
				System.out.println("Registre llegit de la BD (equip): " + equipDadesExtresLlegitDeLaBD.getEquip().toString());
			
				System.out.println("Esborrem el equipDadesExtresLlegitDeLaBD i en cascada l'objecte Equip associat");
				elMeuSession.delete(equipDadesExtresLlegitDeLaBD);
			} else {
				System.out.println("No s'ha trobat cap equip amb id = 2");
			}
			elMeuSession.getTransaction().commit();
			
			if (equipLlegitDeLaBD != null) {
				System.out.println("Acabem la transaccio 2. Registre DELETE en la BD");
			} else {
				System.out.println("Acabem la transaccio 2. No s'ha fet cap DELETE en la BD");
			}
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}
	}

}
