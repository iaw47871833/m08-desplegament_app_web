package acces_dades;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class obtenirDadesEquip {

	public static void main(String[] args) {
		System.out.println("Creem l'objecte SessionFactory");
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Equip.class)
				.addAnnotatedClass(EquipDadesExtres.class)
				.buildSessionFactory();
		System.out.println("Creem l'objecte Session");
		Session elMeuSession = elMeuFactory.openSession();
		
		try {
			System.out.println("Iniciem la transacció");
			elMeuSession.beginTransaction();
			
			System.out.println("BUSQUEDA Equip --> EquipDadesExtres:");
			Equip equipLlegitDeLaBD = elMeuSession.get(Equip.class, 2);
			
			if (equipLlegitDeLaBD != null) {
				System.out.println("Registre llegit de la BD (equip): " + equipLlegitDeLaBD.getNom() + "(id: " + equipLlegitDeLaBD.getId());
				System.out.println("Registre llegit de la BD (equip_dades_extres): " + equipLlegitDeLaBD.getEquipDadesExtres());
			} else {
				System.out.println("No s'ha trobat cap equip amb id = 2");
			}
			elMeuSession.getTransaction().commit();
			
			if (equipLlegitDeLaBD != null) {
				System.out.println("Acabem la transaccio. Exit amb el SELECT en la BD");
			} else {
				System.out.println("Acabem la transaccio. No s'ha fet cap SELECT en la BD");
			}
			
			System.out.println("");
			
			System.out.println("Iniciem la 2a transacció.");
			
			elMeuSession.beginTransaction();
			
			System.out.println("BUSQUEDA EquipDadesEXtres --> Equip:");
			EquipDadesExtres equipDadesExtresLlegitDeLaBD = elMeuSession.get(EquipDadesExtres.class, 2);
			
			if (equipDadesExtresLlegitDeLaBD != null) {
				System.out.println("Registre llegit de la BD (equip_dades_extres): " + equipDadesExtresLlegitDeLaBD.toString());
				System.out.println("Registre llegit de la BD (equip): " + equipDadesExtresLlegitDeLaBD.getEquip().toString());
			} else {
				System.out.println("No s'ha trobat cap equip amb id = 2");
			}
			elMeuSession.getTransaction().commit();
			
			if (equipLlegitDeLaBD != null) {
				System.out.println("Acabem la transaccio 2. Exit amb el SELECT en la BD");
			} else {
				System.out.println("Acabem la transaccio 2. No s'ha fet cap SELECT en la BD");
			}
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}
	}

}
