package acces_dades;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class InsertarEquip {

	public static void main(String[] args) {
		System.out.println("Creem l'objecte SessionFactory");
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Equip.class)
				.addAnnotatedClass(EquipDadesExtres.class)
				.buildSessionFactory();
		System.out.println("Creem l'objecte Session");
		Session elMeuSession = elMeuFactory.openSession();
		
		System.out.println("Creem 1 objecte de tipus Departament i 1 de tipus DepartamentDadesEXtres");
		Equip equipTmp1 = new Equip("Los Angeles Lakers", "Los Angeles");
		EquipDadesExtres equipDadesExtresTmp1 = new EquipDadesExtres("Frank Vogel", "Wish");
		
		// Associem els 2 objectes:
		equipTmp1.setEquipDadesExtres(equipDadesExtresTmp1);
		
		try {
			System.out.println("Iniciem la transacció");
			elMeuSession.beginTransaction();
			elMeuSession.save(equipTmp1);
			elMeuSession.getTransaction().commit();
			System.out.println("Acabem la transacció. Registre INSERTAT en la BD.");
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}
	}

}
