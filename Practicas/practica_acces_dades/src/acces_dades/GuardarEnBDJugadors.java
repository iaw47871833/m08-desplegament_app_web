package acces_dades;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class GuardarEnBDJugadors {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Creem l'objecte SessionFactory");
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Jugadors.class).buildSessionFactory();
		System.out.println("Creem l'objecte Session");
		Session elMeuSession = elMeuFactory.openSession();
		System.out.println("Creem l'objecte de tipus Jugadors");
		Jugadors jugador_1 = new Jugadors("Kyrie", "Irving", "Base", "Brooklyn Nets");
		Jugadors jugador_2 = new Jugadors("James", "Harden", "Base", "Brooklyn Nets");
	
		try {
			System.out.println("Iniciem la transacció");
			elMeuSession.beginTransaction();
			elMeuSession.save(jugador_1);
			elMeuSession.flush();
			elMeuSession.clear();
			elMeuSession.save(jugador_2);
			elMeuSession.getTransaction().commit();
			System.out.println("Acabem la transacció de Kyrie Irving. Registre INSERTAT en la BD.");
			System.out.println("Acabem la transacció de James Harden. Registre INSERTAT en la BD.");
		
			elMeuSession.beginTransaction();
			
			System.out.println("Lectura del registre amb id = " + jugador_1.getId());
			
			Jugadors jugadorLlegitDeLaBD = elMeuSession.get(Jugadors.class, jugador_1.getId());
			
			System.out.println("Registre llegit de la BD: " + jugadorLlegitDeLaBD);
			
			elMeuSession.getTransaction().commit();
			
			System.out.println("Hem acabat de llegit un registre de la BD.");
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}
	}

}
