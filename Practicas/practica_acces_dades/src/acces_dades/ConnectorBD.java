package acces_dades;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectorBD {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String servidorBDUrl = "jdbc:mysql://localhost:3306/basketball?useSSL=false&serverTimezone=UTC";
		String usuari = "root";
		String contrasenya = "password";
		
		System.out.println("INICIANT CONNEXIO AMB LA BD: " + servidorBDUrl);
		
		try {
			Connection conexio = DriverManager.getConnection(servidorBDUrl, usuari, contrasenya);
			System.out.println("Connexio amb exit");
		} catch (SQLException e) {
			System.err.println("e.getMessage() = " + e.getMessage());
			e.printStackTrace();
		}
	}

}
