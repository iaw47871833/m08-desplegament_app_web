package acces_dades;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="equip")
public class Equip {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name="nom")
	private String nom;
	@Column(name="ciutat")
	private String ciutat;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "id")
	EquipDadesExtres equipDadesExtres;
	
	@OneToMany(mappedBy="equip", cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	private List<Entrenador> llistaEntrenadors;
	
	public Equip() {
	}

	/**
	 * @param nom
	 * @param ciutat
	 */
	public Equip(String nom, String ciutat) {
		this.nom = nom;
		this.ciutat = ciutat;
	}
	
	public void afegirEntrenador(Entrenador elNouEntrenador) {
		if (llistaEntrenadors == null) {
			llistaEntrenadors = new ArrayList<Entrenador>();
		}
		llistaEntrenadors.add(elNouEntrenador);
		elNouEntrenador.setEquip(this);
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the ciutat
	 */
	public String getCiutat() {
		return ciutat;
	}

	/**
	 * @param ciutat the ciutat to set
	 */
	public void setCiutat(String ciutat) {
		this.ciutat = ciutat;
	}

	/**
	 * @return the equipDadesExtres
	 */
	public EquipDadesExtres getEquipDadesExtres() {
		return equipDadesExtres;
	}

	/**
	 * @param equipDadesExtres the equipDadesExtres to set
	 */
	public void setEquipDadesExtres(EquipDadesExtres equipDadesExtres) {
		this.equipDadesExtres = equipDadesExtres;
	}

	/**
	 * @return the llistaEntrenadors
	 */
	public List<Entrenador> getLlistaEntrenadors() {
		return llistaEntrenadors;
	}

	/**
	 * @param llistaEntrenadors the llistaEntrenadors to set
	 */
	public void setLlistaEntrenadors(List<Entrenador> llistaEntrenadors) {
		this.llistaEntrenadors = llistaEntrenadors;
	}

	@Override
	public String toString() {
		return "Equip [id=" + id + ", nom=" + nom + ", ciutat=" + ciutat + "]";
	}
}
