package acces_dades;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class EsborrarEquipUnAUnUnidireccional {

	public static void main(String[] args) {
		System.out.println("Creem l'objecte SessionFactory");
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Equip.class)
				.addAnnotatedClass(EquipDadesExtres.class)
				.buildSessionFactory();
		System.out.println("Creem l'objecte Session");
		Session elMeuSession = elMeuFactory.openSession();
		
		try {
			System.out.println("Iniciem la transacció");
			elMeuSession.beginTransaction();
			Equip equipLlegitDeLaBD = elMeuSession.get(Equip.class, 1);
			
			if (equipLlegitDeLaBD != null) {
				System.out.println("Registre llegit de la BD: " + equipLlegitDeLaBD.getNom() + "(id: " + equipLlegitDeLaBD.getId());
				
				elMeuSession.delete(equipLlegitDeLaBD);
			} else {
				System.out.println("No s'ha trobat cap equip amb id = 1");
			}
			elMeuSession.getTransaction().commit();
			
			if (equipLlegitDeLaBD != null) {
				System.out.println("Acabem la transaccio. Registre DELETE en la BD");
			} else {
				System.out.println("Acabem la transaccio. No s'ha fet cap DELETE en la BD");
			}
			
			System.out.println("Acabem la transacció. Registre INSERTAT en la BD.");
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}
	}

}
