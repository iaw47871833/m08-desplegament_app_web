package acces_dades;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="equip_dades_extres")
public class EquipDadesExtres {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name="entrenador")
	private String entrenador;
	@Column(name="patrocinador")
	private String patrocinador;
	@OneToOne(mappedBy = "equipDadesExtres", cascade=CascadeType.ALL)
	Equip equip;
	
	/**
	 * 
	 */
	public EquipDadesExtres() {
	}
	/**
	 * @param entrenador
	 * @param patrocinador
	 */
	public EquipDadesExtres(String entrenador, String patrocinador) {
		this.entrenador = entrenador;
		this.patrocinador = patrocinador;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the entrenador
	 */
	public String getEntrenador() {
		return entrenador;
	}
	/**
	 * @param entrenador the entrenador to set
	 */
	public void setEntrenador(String entrenador) {
		this.entrenador = entrenador;
	}
	/**
	 * @return the patrocinador
	 */
	public String getPatrocinador() {
		return patrocinador;
	}
	/**
	 * @param patrocinador the patrocinador to set
	 */
	public void setPatrocinador(String patrocinador) {
		this.patrocinador = patrocinador;
	}
	
	/**
	 * @return the equip
	 */
	public Equip getEquip() {
		return equip;
	}
	/**
	 * @param equip the equip to set
	 */
	public void setEquip(Equip equip) {
		this.equip = equip;
	}
	@Override
	public String toString() {
		return "EquipDadesExtres [id=" + id + ", entrenador=" + entrenador + ", patrocinador=" + patrocinador + "]";
	}
}
