-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 15, 2021 at 09:21 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `basketball`
--

-- --------------------------------------------------------

--
-- Table structure for table `entrenador`
--

CREATE TABLE `entrenador` (
  `id` int(3) NOT NULL,
  `nom` varchar(150) DEFAULT NULL,
  `cognom` varchar(150) DEFAULT NULL,
  `equipId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `entrenador`
--

INSERT INTO `entrenador` (`id`, `nom`, `cognom`, `equipId`) VALUES
(1, 'Frank', 'Vogel', NULL),
(2, 'Terry', 'Sttots', NULL),
(3, 'Greg', 'Popovich', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `equip`
--

CREATE TABLE `equip` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `ciutat` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `equip`
--

INSERT INTO `equip` (`id`, `nom`, `ciutat`) VALUES
(4, 'Los Angeles Lakers', 'Los Angeles');

-- --------------------------------------------------------

--
-- Table structure for table `equip_dades_extres`
--

CREATE TABLE `equip_dades_extres` (
  `id` int(11) NOT NULL,
  `entrenador` varchar(100) NOT NULL,
  `patrocinador` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `equip_dades_extres`
--

INSERT INTO `equip_dades_extres` (`id`, `entrenador`, `patrocinador`) VALUES
(4, 'Frank Vogel', 'Wish');

-- --------------------------------------------------------

--
-- Table structure for table `jugadors`
--

CREATE TABLE `jugadors` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `cognom` varchar(100) NOT NULL,
  `posicio` varchar(50) NOT NULL,
  `equip` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jugadors`
--

INSERT INTO `jugadors` (`id`, `nom`, `cognom`, `posicio`, `equip`) VALUES
(1, 'Kyrie', 'Irving', 'Base', 'Brooklyn Nets'),
(2, 'James', 'Harden', 'Base', 'Houston Rockets');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `entrenador`
--
ALTER TABLE `entrenador`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_EQUIP_ID` (`equipId`);

--
-- Indexes for table `equip`
--
ALTER TABLE `equip`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_equip_dades_extres` (`id`);

--
-- Indexes for table `equip_dades_extres`
--
ALTER TABLE `equip_dades_extres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jugadors`
--
ALTER TABLE `jugadors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `entrenador`
--
ALTER TABLE `entrenador`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `equip`
--
ALTER TABLE `equip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `equip_dades_extres`
--
ALTER TABLE `equip_dades_extres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jugadors`
--
ALTER TABLE `jugadors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `entrenador`
--
ALTER TABLE `entrenador`
  ADD CONSTRAINT `FK_EQUIP_ID` FOREIGN KEY (`equipId`) REFERENCES `equip` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `equip`
--
ALTER TABLE `equip`
  ADD CONSTRAINT `FK_DEPARTAMENT_DADES_EXTRES` FOREIGN KEY (`id`) REFERENCES `equip_dades_extres` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
