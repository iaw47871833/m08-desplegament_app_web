# INICI

### Que es Spring?

Es un **framework** en un entrorn de treball que consta d'unes pautes a seguir i unes eines a través de llibreries.

Spring es va crear per a desenvolupar aplicacions JEE(Java Enterprise Edition) de manera mésràpida i fàcil ja que fins a llavors la implementació dels  EJB(Enterprise JavaBeans)  era moltcostosa en temps, dificultat i consum de recursos.

### Avantatges de fer servir Spring

* La injecció de dependències la qual permet que existeixi un loose coupling (baix acoplament) demanera que si s'ha de fer un mateix canvi en un programa que afecti a diverses classes ara noméss'haurà de de fer el canvi  en 1 lloc.

* POJO's (Plain Old Java Objects, objectes sencills antics de java) que nosón més que classes sencilles  de java que no hereten de cap classe i que no implementen capinterface.

* Simplifica l'accés a les bases de dades.

* Programació orientada a aspectes (AOP) la qual permet modularitzar el codi d'una classe per aseparar les diverses tareees que ha de fer la classe.

### Estructura del core (nucli) de Spring

#### Contenidor central

- Creació de beans- Lectura d'arxius de configuració
- Ús de les propietats i dependències 
- Ùs del Context on s'emmagatezem els beans en memòria
-   SpEL   (Spring   Expression   Language)   que   és   un   llenguatge   d'expressions   que   ens   permetràmanipular objectes en temps d'execució

#### Infraestructura

- S'encarrega de les transaccions¹, els logeigs i de la seguretat de l'aplicació

#### Accés a dades

Permet accedir a tot tipus de bases de dades.

#### Web / MVC (Model Vista Controlador)
