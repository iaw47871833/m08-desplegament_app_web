<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Confirmació d'alta d'un nou tripulant - CCCP Leonov</title>
</head>
<body>
	S'ha donat d'alta el tripulant <b> ${el_doctor.nom} ${el_doctor.cognom} </b> el qual té <b> ${el_doctor.edat} </b> anys.
	<br/><br/>
	Email del nou tripulant: <b> ${el_doctor.email} </b>
	<br/>
	El departament del nou tripulant és <b> ${el_doctor.departament}</b>
	<br/>
	Els coneixements del nou tripulant són <b> ${el_doctor.conneixements} </b>
	<br/>
	La ciutat de naixement del nou tripulant és <b> ${el_doctor.ciutatNaixement} </b>
	<br/>
	Els idiomes que sap del nou tripulant són <b> ${el_doctor.idiomes}</b> 
	<br/>
	El codi postal del nou tripulant és <b> ${el_doctor.codiPostal}</b>
</body>
</html>