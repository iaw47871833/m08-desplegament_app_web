package curs_de_05_AplicacionesWeb;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Tripulant {
	@NotNull
	@Size(min = 1, message = "Long. mínima de nom ha de ser 1 caràcter.")
	private String nom;
	private String cognom;
	@NotNull
	@Min(value = 18, message = "El nou tripulant ha de tenir com a mínim 18 anys.")
	@Max(value = 65, message = "El nou tripulant ha de tenir com a màxim 18 anys.")
	private int edat;
	@NotBlank(message = "S'ha d'omplit l'email.")
	@Email
	private String email;
	private String departament;
	private String conneixements;
	private String ciutatNaixement;
	private String idiomes;
	@Pattern(regexp="[0-9]{5}", message = "Només 5 números")
	private String codiPostal;
	
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the cognom
	 */
	public String getCognom() {
		return cognom;
	}
	/**
	 * @param cognom the cognom to set
	 */
	public void setCognom(String cognom) {
		this.cognom = cognom;
	}
	
	/**
	 * @return the edat
	 */
	public int getEdat() {
		return edat;
	}

	/**
	 * @param edat the edat to set
	 */
	public void setEdat(int edat) {
		this.edat = edat;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the departament
	 */
	public String getDepartament() {
		return departament;
	}
	/**
	 * @param departament the departament to set
	 */
	public void setDepartament(String departament) {
		this.departament = departament;
	}

	/**
	 * @return the conneixements
	 */
	public String getConneixements() {
		return conneixements;
	}

	/**
	 * @param conneixements the conneixements to set
	 */
	public void setConneixements(String conneixements) {
		this.conneixements = conneixements;
	}

	/**
	 * @return the ciutatNaixement
	 */
	public String getCiutatNaixement() {
		return ciutatNaixement;
	}

	/**
	 * @param ciutatNaixement the ciutatNaixement to set
	 */
	public void setCiutatNaixement(String ciutatNaixement) {
		this.ciutatNaixement = ciutatNaixement;
	}

	/**
	 * @return the idiomes
	 */
	public String getIdiomes() {
		return idiomes;
	}

	/**
	 * @param idiomes the idiomes to set
	 */
	public void setIdiomes(String idiomes) {
		this.idiomes = idiomes;
	}

	/**
	 * @return the codiPostal
	 */
	public String getCodiPostal() {
		return codiPostal;
	}

	/**
	 * @param codiPostal the codiPostal to set
	 */
	public void setCodiPostal(String codiPostal) {
		this.codiPostal = codiPostal;
	}
	
}
