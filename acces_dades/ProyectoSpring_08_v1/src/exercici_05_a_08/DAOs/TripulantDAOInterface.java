package exercici_05_a_08.DAOs;

import java.util.List;

import exercici_05_08.entitats.Tripulant;

public interface TripulantDAOInterface {
	// Aquest mètode retornarà una llista dels tripulants que hi ha en la BD
	public List<Tripulant> getTripulants();
}
