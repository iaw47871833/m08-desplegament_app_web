package exercici_05_a_08.DAOs;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import exercici_05_08.entitats.Tripulant;

public class TripulantDAOClasse implements TripulantDAOInterface {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	@Transactional
	public List<Tripulant> getTripulants() {
		Session laMevaSessio = sessionFactory.getCurrentSession();
		
		Query<Tripulant> queryLlistaTripulants = laMevaSessio.createQuery("from Tripulant", Tripulant.class);
		
		List<Tripulant> llistaTripulantsDeLaBD = queryLlistaTripulants.getResultList();
		
		return llistaTripulantsDeLaBD;
	}

}
