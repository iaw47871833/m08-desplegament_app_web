package exercici_05_a_08.controladors;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import exercici_05_a_08.DAOs.*;
import exercici_05_a_08.entitats.Tripulant;

@Controller
@RequestMapping("/tripulant")
public class Controlador {

	@Autowired
	private TripulantDAOInterface tripulantDAO;

	@RequestMapping("/llistaTripulants")
	public String llistarTripulants(Model elModel) {

		List<Tripulant> llistaTripulantsDelDAO = tripulantDAO.getTripulants();
		elModel.addAttribute("llistaTripulants", llistaTripulantsDelDAO);

		return "llista_tripulants";
	}

	@RequestMapping("/formulariInsertTripulant")
	public String mostrarFormulariInsertTripulant(Model elModel) {
		Tripulant nouTripulant = new Tripulant();
		elModel.addAttribute("elNouTripulant", nouTripulant);
		return "formulari_insertar_tripulant";
	}

	@PostMapping("/procesarAltaTripulant")
	public String donarDAltaTripulant(@ModelAttribute("elNouTripulant") Tripulant tripulantNou) {
		// Connectem l'objecte 'tripulantNou' amb l'objecte 'elNouTripulant' que està //
		// associat al formulari de la vista 'formulari_insertar_tripulant'.// Insertem
		// el nou tripulant ('tripulantNou') en la BD:// Dona un error perquè no
		// existeix el mètode 'insertarTripulant' --> clickem en // l'error i clickem en
		// "Create method...".// Després haurem d'anar a la classe TripulantDAO i haurem
		// d'implementar el mètode.
		tripulantNou.setDepartamentId(2);
		tripulantNou.setDataCreacio(LocalDateTime.now());
		tripulantDAO.insertarTripulant(tripulantNou);//
		// Després de fer l'insert, retorna a la vista on veiem en una llista els
		// tripulants // que hi ha en la BD.
		return "redirect:/tripulant/llistaTripulants";
	}

	@GetMapping("formulariUpdateTripulant")
	public String mostrarFormulariUpdateTripulant(@RequestParam("tripulantId") int tripulantId, Model elModel) {
		// Obtenir el tripulant amb id = tripulantId fent servir el DAO
		// 'TripulantDAOInterface'.
		//
		// Com que no existeix el mètode 'getTripulant(int)' en la interface
		// 'TripulantDAOInterface'
		// surirà un error que es soluciona seleccionant "Create method...en la
		// interface..." i
		// després implementant el mètode en la classe 'TripulantDAOClasse'.
		Tripulant tripulantTmp = tripulantDAO.getTripulant(tripulantId);
		// Afegir el tripulant al model perquè la jsp 'formulari_updatejar_tripulant' hi
		// tingui
		// accés.
		elModel.addAttribute("tripulantPerUpdatejar", tripulantTmp);
		return "formulari_updatejar_tripulant";
	}

	@PostMapping("/procesarUpdateTripulant") 
	public String updatejarTripulant(@ModelAttribute("tripulantPerUpdatejar") Tripulant tripulantPerUpdatejar) {
		tripulantDAO.updatejarTripulant(tripulantPerUpdatejar);
		// Després de fer l'insert, retorna a la vista on veiem en una llista els 
		// tripulants que hi ha en la BD.
		return"redirect:/tripulant/llistaTripulants";
	}

	@GetMapping("formulariDeleteTripulant")public String deleteTripulant(@RequestParam("tripulantId") int tripulantId) {
		// No fa falta rebre el Model perquè per esborrar al tripulant tinc prou amb el
		// seu id.
		//
		// Eliminar el tripulant amb id = tripulantId fent servir el DAO
		// 'TripulantDAOInterface'.
		//
		// Com que no existeix el mètode 'eliminarTripulant(int)' en la interface
		// 'TripulantDAOInterface' surtirà un error que es soluciona seleccionant "Create
		// method...en la interface..." i després implementant el mètode en la classe
		// 'TripulantDAOClasse'.
		tripulantDAO.eliminarTripulant(tripulantId);
		return"redirect:/tripulant/llistaTripulants"; 
	}
	
}
