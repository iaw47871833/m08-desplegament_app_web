package exercici_05_a_08.DAOs;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import exercici_05_a_08.entitats.Tripulant;

@Repository
public class TripulantDAOClasse implements TripulantDAOInterface {

	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	@Override
	public List<Tripulant> getTripulants() {

		Session laMevaSessio = sessionFactory.getCurrentSession();
		Query<Tripulant> queryLlistaTripulants = laMevaSessio.createQuery("from Tripulant", Tripulant.class);

		List<Tripulant> llistaTripulantDeLaBD = queryLlistaTripulants.getResultList();

		return llistaTripulantDeLaBD;
	}

	@Override
	@Transactional
	public void insertarTripulant(Tripulant tripulantNou) {
		// Obtenir la session:
		Session laMevaSessio = sessionFactory.getCurrentSession();
		// Gravem el tripulant en la BD:
		laMevaSessio.save(tripulantNou);
	}

	@Override
	@Transactional
	public Tripulant getTripulant(int tripulantId) {
		// Obtenir la session:
		Session laMevaSessio = sessionFactory.getCurrentSession();
		// Obtenir la informació del tripulant amb id = tripulantID.
		// A través del mapeig ORM consultarà la BD i retornarà el tripulant amb id =
		// tripulantID.
		Tripulant tripulantTmp = laMevaSessio.get(Tripulant.class, tripulantId);
		/*
		 * SUPOSO QUE EL CODI D'ADALT FA LO MATEIX QUE EL SEGÜENT CODI: // Crear la
		 * consulta (Query): // La variable de tipus Query serà el DAO (Data Acess
		 * Object). Query<Tripulant> queryTripulantVsID =
		 * laMevaSessio.createQuery("from Tripulant where id = " + tripulantId,
		 * Tripulant.class); // Executar la query i retornar el resultat. Tripulant
		 * tripulantTmp = queryTripulantVsID.getSingleResult();
		 */
		return tripulantTmp;
	}

	@Override
	@Transactional
	public void updatejarTripulant(Tripulant tripulantPerUpdatejar) {
		// Obtenir la session:
		Session laMevaSessio = sessionFactory.getCurrentSession();
		// Updatejem el tripulant en la BD:
		//laMevaSessio.saveOrUpdate(tripulantPerUpdatejar);
		laMevaSessio.update(tripulantPerUpdatejar);
	}
	
	@Override
	@Transactional
	public void eliminarTripulant(int tripulantId) {
		// Obtenir la session:
		Session laMevaSessio = sessionFactory.getCurrentSession();
		Query queryEsborrarTripulant = laMevaSessio.createQuery("DELETE from Tripulant where id = " + tripulantId); 
		queryEsborrarTripulant.executeUpdate();
	}
}
