package exercici_05_a_08.DAOs;

import java.util.List;
import exercici_05_a_08.entitats.Tripulant;

public interface TripulantDAOInterface {

	public List<Tripulant> getTripulants();

	void insertarTripulant(Tripulant tripulantNou);
	
	public Tripulant getTripulant(int tripulantId);
	
	public void updatejarTripulant(Tripulant tripulantPerUpdatejar);
	
	public void eliminarTripulant(int tripulantId);
}
