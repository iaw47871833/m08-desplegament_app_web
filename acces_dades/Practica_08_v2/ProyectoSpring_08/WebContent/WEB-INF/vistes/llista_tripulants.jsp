<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>LLista de tripulants</title>
<link rel="stylesheet" type="text/css"
	href="/recursos/css/elMeuEstil.css">
</head>
<body>
	<h1>LLista de tripulants</h1>
	<table>
		<tr>
			<th>ID</th>
			<th>Nom</th>
			<th>Cognom</th>
			<th>Email</th>
			<th>Data alta</th>
			<th>Departament ID</th>
		</tr>
		<c:forEach var="tripulantTmp" items="${llistaTripulants}">
			<!-- L'atribut 'var' serà el nom que tindrà el tag 'url' per a poder agafar-lo en la part del codi on fem la última columna de la taula i 'value' serà el valor que tindrà. -->
			<c:url var="linkUpdatejar"
				value="/tripulant/formulariUpdateTripulant">
				<c:param name="tripulantId" value="${tripulantTmp.id}"></c:param>
			</c:url>

			<!-- Link (mapejat en el Controlador) per esborrar al tripulant. -->
			<c:url var="linkDelete" value="/tripulant/formulariDeleteTripulant">
				<c:param name="tripulantId" value="${tripulantTmp.id}"></c:param>
			</c:url>
			<tr>
				<td>${tripulantTmp.id}</td>
				<td>${tripulantTmp.nom}</td>
				<td>${tripulantTmp.cognom}</td>
				<td>${tripulantTmp.email}</td>
				<td>${tripulantTmp.dataCreacio}</td>
				<td>${tripulantTmp.departamentId}</td>
				<td><a href="${linkUpdatejar}"> <input type="button"
						value="Updatejar">
				</a></td>
				<td>
					<a href="${linkDelete}">
						<input type="button" value="Delete" onclick="if(!(confirm('Estàs segur d'esborrar al tripulant?'))) return false">
					</a>
				</td>
			</tr>
		</c:forEach>
	</table>

	<input type="button" value="Afegir tripulant a la BD"
		onclick="window.location.href='formulariInsertTripulant'; return false;" />
</body>
</html>