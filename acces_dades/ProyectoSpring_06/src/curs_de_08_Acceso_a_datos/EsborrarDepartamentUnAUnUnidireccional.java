package curs_de_08_Acceso_a_datos;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class EsborrarDepartamentUnAUnUnidireccional {

	public static void main(String[] args) {
		// Creem l'objecte SessionFactory i li afegim les 2 classes amb les que volem treballar
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml")
											.addAnnotatedClass(Departament.class)
											.addAnnotatedClass(DepartamentDadesExtres.class)
											.buildSessionFactory();
		System.out.println("Creem l'objecte Session.");
		Session elMeuSession = elMeuFactory.openSession();
		
		try {
			// Esborrem un objecte de tipus Departament i com que aquest está connectat amb la classe DepartamentDadesEXtres
			// amb la relació un a unn (Annotations @OneToOne i @JoinColumn), també s'esborrarà automàticament un objecte de
			// tipus DepartamentDadesExtres de la BD (el que estigui associat amb l'objecte de tipus Departament que esborrem).
			System.out.println("Iniciem la transaccio");
			
			elMeuSession.beginTransaction();
			
			// Voldrem esborrar el departament amb id = 3. Busquem en la BD un departament amb id=3.
			// Si no trobés cap departament amb id = 3, retornarà NULL i per tant departamentLlegitDeLaBD = NULL
			Departament departamentLlegitDeLaBD = elMeuSession.get(Departament.class, 1);
			
			if (departamentLlegitDeLaBD != null) {
				System.out.println("Registre llegit de la BD: " + departamentLlegitDeLaBD.getNom() + " (id: " + departamentLlegitDeLaBD.getId() + ").");
				elMeuSession.delete(departamentLlegitDeLaBD);
			} else {
				System.out.println("No s'ha trobat cap departament amb id = 1.");
			}
			
			elMeuSession.getTransaction().commit();
			if (departamentLlegitDeLaBD != null) {
				System.out.println("Acabem la transacció. Registre DELETE en la BD.");
			} else {
				System.out.println("Acabem la transacció. No s'ha fet cap DELETE en la BD.");
			}
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}
	}

}
