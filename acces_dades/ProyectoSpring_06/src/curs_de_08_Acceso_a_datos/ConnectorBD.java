package curs_de_08_Acceso_a_datos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectorBD {

	public static void main(String[] args) {
		// Configurem la conexió a la base de dades
		String servidorBDUrl = "jdbc:mysql://localhost:3307/LeonovBD?useSSL=false";
		String usuari = "root";
		String contrasenya = "password";
		System.out.println("INICIANT CONNEXIO AMB LA BD: " + servidorBDUrl);
		
		try {
			// Conectem a la BD
			Connection conexio = DriverManager.getConnection(servidorBDUrl, usuari, contrasenya);
			System.out.println("CONNEXIO AMB EXIT");
		} catch(SQLException e) {
			System.out.println("e.getMessage() = " + e.getMessage());
			e.printStackTrace();
		}
	}

}
