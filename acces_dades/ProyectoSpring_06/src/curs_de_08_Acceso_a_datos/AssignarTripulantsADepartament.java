package curs_de_08_Acceso_a_datos;

import java.time.LocalDateTime;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class AssignarTripulantsADepartament {

	public static void main(String[] args) {
		// Creem l'objecte SessionFactory i li afegim les 2 classes amb les que volem treballar
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml")
											.addAnnotatedClass(Departament.class)
											.addAnnotatedClass(DepartamentDadesExtres.class)
											.buildSessionFactory();
		System.out.println("Creem l'objecte Session.");
		Session elMeuSession = elMeuFactory.openSession();
		
		try {
			// Insertem un objecte de tipus Departament i com que aquest está connectat amb la classe DepartamentDadesExtres
			// amb la relació un a un (Annotations @OneToOne i @JoinColumn), també s'insertarà automàticament un objecte de
			// tipus DepartamentDadesExtres en la BD.
			System.out.println("Iniciem la transaccio");
			
			elMeuSession.beginTransaction();
			
			// Agafem de la BD el departament amb id = 3
			Departament departamentLlegitDelaBD = elMeuSession.get(Departament.class, 3);
			
			// Creem 3 tripulants per afegir-los al departament amb id = 3
			Tripulant tripulant_1 = new Tripulant("Tanya", "Kirbuk", LocalDateTime.now());
			Tripulant tripulant_2 = new Tripulant("Vasili", "Orlov", LocalDateTime.now());
			Tripulant tripulant_3 = new Tripulant("Heywood", "R.Floy", LocalDateTime.now());
			
			// Assignem els 3 tripulants al departament amb id = 3
			departamentLlegitDelaBD.afegirTripulant(tripulant_1);
			departamentLlegitDelaBD.afegirTripulant(tripulant_2);
			departamentLlegitDelaBD.afegirTripulant(tripulant_3);
			
			// Guardem els tripulants en la taula 'tripulant' de la BD
			elMeuSession.save(tripulant_1);
			elMeuSession.save(tripulant_2);
			elMeuSession.save(tripulant_3);
			
			elMeuSession.getTransaction().commit();
			
			
			System.out.println("Acabem la transacció. Registre INSERTAT en la BD.");
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}
	}

}
