package curs_de_08_Acceso_a_datos;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class InsertarDepartament {

	public static void main(String[] args) {
		// Creem l'objecte SessionFactory i li afegim les 2 classes amb les que volem treballar
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml")
											.addAnnotatedClass(Departament.class)
											.addAnnotatedClass(DepartamentDadesExtres.class)
											.buildSessionFactory();
		System.out.println("Creem l'objecte Session.");
		Session elMeuSession = elMeuFactory.openSession();
		
		System.out.println("Creem 1 objecte de tipus Departament i 1 de tipus DepartamentDadesExtres.");
		Departament departamentTmp1 = new Departament("Navegacio", "navegacio@leonov.com", "coberta 3, seccio 1, departament 7");
		DepartamentDadesExtres departamentDadesExtresTmp1 = new DepartamentDadesExtres("http://localhost/navegacio", "358928545793", "És el departament 1");
	
		// Associem els 2 objectes:
		departamentTmp1.setDepartamentDadesExtres(departamentDadesExtresTmp1);
		
		try {
			// Insertem un objecte de tipus Departament i com que aquest está connectat amb la classe DepartamentDadesExtres
			// amb la relació un a un (Annotations @OneToOne i @JoinColumn), també s'insertarà automàticament un objecte de
			// tipus DepartamentDadesExtres en la BD.
			System.out.println("Iniciem la transaccio");
			
			elMeuSession.beginTransaction();
			elMeuSession.save(departamentTmp1);
			elMeuSession.getTransaction().commit();
			
			System.out.println("Acabem la transacció. Registre INSERTAT en la BD.");
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}
	}

}
