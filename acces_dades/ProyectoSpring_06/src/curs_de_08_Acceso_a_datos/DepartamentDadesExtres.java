package curs_de_08_Acceso_a_datos;

import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="departament_dades_extres")
public class DepartamentDadesExtres {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name="web")
	private String web;
	@Column(name="telefon")
	private String telefon;
	@Column(name="descripcio")
	private String descripcio;
	@OneToOne(mappedBy = "departamentDadesExtres", cascade=CascadeType.ALL)
	Departament departament;
	
	/**
	 * Constructor sense parametres
	 */
	public DepartamentDadesExtres() {
	}

	/**
	 * @param web
	 * @param telefon
	 * @param text
	 */
	public DepartamentDadesExtres(String web, String telefon, String descripcio) {
		this.web = web;
		this.telefon = telefon;
		this.descripcio = descripcio;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the web
	 */
	public String getWeb() {
		return web;
	}

	/**
	 * @param web the web to set
	 */
	public void setWeb(String web) {
		this.web = web;
	}

	/**
	 * @return the telefon
	 */
	public String getTelefon() {
		return telefon;
	}

	/**
	 * @param telefon the telefon to set
	 */
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	/**
	 * @return the text
	 */
	public String getDescripcio() {
		return descripcio;
	}

	/**
	 * @param text the text to set
	 */
	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	/**
	 * @return the departament
	 */
	public Departament getDepartament() {
		return departament;
	}

	/**
	 * @param departament the departament to set
	 */
	public void setDepartament(Departament departament) {
		this.departament = departament;
	}

	@Override
	public String toString() {
		return "DepartamentDadesExtres [id=" + id + ", web=" + web + ", telefon=" + telefon + ", descripcio="
				+ descripcio + ", departament=" + departament + "]";
	}	
}
