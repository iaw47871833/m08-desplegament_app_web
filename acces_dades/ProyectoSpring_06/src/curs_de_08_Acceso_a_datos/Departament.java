package curs_de_08_Acceso_a_datos;

import java.util.ArrayList;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="departament")
public class Departament {
	// Atributs
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name="nom")
	private String nom;
	@Column(name="email")
	private String email;
	@Column(name="direccio")
	private String direccio;
	
	// Indicar clau foranea
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="id")
	DepartamentDadesExtres departamentDadesExtres;
	@OneToMany(fetch=FetchType.EAGER, mappedBy="departament", cascade= {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	private List<Tripulant> llistaTripulants;

	/**
	 * Constructor sense arguments
	 */
	public Departament() {
	}

	/**
	 * @param nom
	 * @param email
	 * @param direccio
	 */
	public Departament(String nom, String email, String direccio) {
		this.nom = nom;
		this.email = email;
		this.direccio = direccio;
	}
	
	/**
	 * afegirTripulant
	 * 
	 * Afegir un nou tripulant al departament
	 */
	
	public void afegirTripulant(Tripulant elNouTripulant) {
		if (llistaTripulants == null) {
			llistaTripulants = new ArrayList<Tripulant>();
		}
		llistaTripulants.add(elNouTripulant);
		elNouTripulant.setDepartament(this);
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the direccio
	 */
	public String getDireccio() {
		return direccio;
	}

	/**
	 * @param direccio the direccio to set
	 */
	public void setDireccio(String direccio) {
		this.direccio = direccio;
	}
	
	/**
	 * @return the departamentDadesExtres
	 */
	public DepartamentDadesExtres getDepartamentDadesExtres() {
		return departamentDadesExtres;
	}

	/**
	 * @param departamentDadesExtres the departamentDadesExtres to set
	 */
	
	public void setDepartamentDadesExtres(DepartamentDadesExtres departamentDadesExtres) {
		this.departamentDadesExtres = departamentDadesExtres;
	}

	/**
	 * @return the llistaTripulants
	 */
	
	public List<Tripulant> getLlistaTripulants() {
		return llistaTripulants;
	}

	/**
	 * @param llistaTripulants the llistaTripulants to set
	 */
	public void setLlistaTripulants(List<Tripulant> llistaTripulants) {
		this.llistaTripulants = llistaTripulants;
	}

	@Override
	public String toString() {
		return "Departament [id=" + id + ", nom=" + nom + ", email=" + email + ", direccio=" + direccio + "]";
	}

	
}
