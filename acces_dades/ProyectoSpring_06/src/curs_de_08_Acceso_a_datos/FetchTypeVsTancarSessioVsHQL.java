package curs_de_08_Acceso_a_datos;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class FetchTypeVsTancarSessioVsHQL {

	public static void main(String[] args) {
		// Creem l'objecte SessionFactory i li afegim les 2 classes amb les que volem treballar
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml")
											.addAnnotatedClass(Departament.class)
											.addAnnotatedClass(DepartamentDadesExtres.class)
											.buildSessionFactory();
		System.out.println("Creem l'objecte Session.");
		Session elMeuSession = elMeuFactory.openSession();
		
		try {
			// Insertem un objecte de tipus Departament i com que aquest está connectat amb la classe DepartamentDadesExtres
			// amb la relació un a un (Annotations @OneToOne i @JoinColumn), també s'insertarà automàticament un objecte de
			// tipus DepartamentDadesExtres en la BD.
			System.out.println("Iniciem la transaccio");
			
			elMeuSession.beginTransaction();
			
			String sentenciaHQL = "SELECTT dept FROM Departament dept JOIN FETCH dept.llistaTripulants WHERE dept.id = 3";
			System.out.println("1. Llançem la consulta '" + "'.");
			
			System.out.println("----- 1 -----");
			
			List<Departament> llistaDepartaments = elMeuSession.createQuery(sentenciaHQL).getResultList();
			
			if (llistaDepartaments.size() > 0) {
				for (Departament DepartamentTmp : llistaDepartaments) {
					System.out.println(DepartamentTmp);
					System.out.println(DepartamentTmp.getLlistaTripulants());
				}
			} else {
				System.out.println("No s'ha trobat cap registre.");
			}
			
			System.out.println("----- 2 -----");
			
			try {
				Departament departament = (Departament) elMeuSession.createQuery(sentenciaHQL).getSingleResult();
				System.out.println(departament);
				System.out.println(departament.getLlistaTripulants());
			} catch(NoResultException e) {
				System.out.println("No s'ha trobat cap registre");
			}
			
			System.out.println("----- 3 -----");
			
			// Una altre forma sense haver de fer servir el getSingleResult() i l'excepció per no trobar cap registre
			// Si ens retorna 0 registres, a llavors ens retornarà NULL.
			
			// Agafem de la BD el departament amb id = 3
			Departament departament2 = (Departament) elMeuSession.createQuery(sentenciaHQL).uniqueResultOptional().orElse(null);
			
			if (departament2 != null) {
				System.out.println(departament2);
				System.out.println(departament2.getLlistaTripulants());
			} else {
				System.out.println("NO S'HA TROBAT CAP DEPARTAMENT");
			}
			
			System.out.println("----- 4 -----");
			
			//elMeuSession.getTransaction().commit();
		} finally {			
			elMeuSession.close();
			elMeuFactory.close();
		}
	}

}
