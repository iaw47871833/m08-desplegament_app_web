package curs_de_08_Acceso_a_datos;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class EsborrarDepartamentUnAUnBidireccional {

	public static void main(String[] args) {
		// Creem l'objecte SessionFactory i li afegim les 2 classes amb les que volem treballar
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml")
											.addAnnotatedClass(Departament.class)
											.addAnnotatedClass(DepartamentDadesExtres.class)
											.buildSessionFactory();
		System.out.println("Creem l'objecte Session.");
		Session elMeuSession = elMeuFactory.openSession();
		
		try {
			// Busquem totes les dades d'un departament fent servir la relació bidireccional
			// A partir d'un objecte Departament s'ha de trobar automàticament l'objecte DepartamentDadesExtres associat i
			// a la inversa també
			System.out.println("Iniciem la 1a transaccio");
			
			elMeuSession.beginTransaction();
			
			// DELETE Departament --> DepartamentDadesExtres:
			System.out.println("DELETE Departament --> DepartamentDadesExtres:");
			Departament departamentLlegitDeLaBD = elMeuSession.get(Departament.class, 2);
			
			if (departamentLlegitDeLaBD != null) {
				System.out.println("Registre llegit de la BD (departament): " + departamentLlegitDeLaBD.toString());
				System.out.println("Registre llegit de la BD (departament_dades_extres): " + departamentLlegitDeLaBD.getDepartamentDadesExtres().toString());
			
				System.out.println("Esborrem el departamentLlegitDeLaBD i en cascada l'objecte DepartamentDadesExtres associat.");
				elMeuSession.delete(departamentLlegitDeLaBD);
			} else {
				System.out.println("No s'ha trobat cap departament amb id = 2.");
			}
			
			elMeuSession.getTransaction().commit();
			
			if (departamentLlegitDeLaBD != null) {
				System.out.println("Acabem la transacció 1. Registre DELETE en la BD.");
			} else {
				System.out.println("Acabem la transacció 1. No s'ha fet cap DELETE en la BD.");
			}
			System.out.println();
			
			System.out.println("Iniciem la 2a transacció.");
			elMeuSession.beginTransaction();
			
			// DELETE DepartamentDadesExtres --> Departament:
			System.out.println("DELETE DepartamentDadesExtres --> Departament:");
			DepartamentDadesExtres departamentDadesExtresLlegitDeLaBD = elMeuSession.get(DepartamentDadesExtres.class, 2);
			
			if(departamentDadesExtresLlegitDeLaBD != null) {
				System.out.println("Registre llegit de la BD (departament_dades_extres): " + departamentDadesExtresLlegitDeLaBD.toString());
				System.out.println("Registre llegit de la BD (departament): " + departamentDadesExtresLlegitDeLaBD.getDepartament().toString());
				
				System.out.println("Esborrem el departamentDadesExtresLlegitDeLaBD i en cascada l'objecte Departament associat.");
			} else {
				System.out.println("No s'ha trobat cap departament amb id = 2");
			}
			
			elMeuSession.getTransaction().commit();
			
			if(departamentDadesExtresLlegitDeLaBD != null) {
				System.out.println("Acabem la transacció 2. Registre DELETE en la BD.");
			} else {
				System.out.println("Acabem la transacció 2. No s'ha fet cap DELETE en la BD.");
			}
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}
	}

}
