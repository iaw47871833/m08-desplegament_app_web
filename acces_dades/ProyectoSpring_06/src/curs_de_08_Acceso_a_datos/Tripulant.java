package curs_de_08_Acceso_a_datos;

import java.time.LocalDateTime;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="tripulant")
public class Tripulant {
	// Atributs
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name="nom")
	private String nom;
	@Column(name="cognom")
	private String cognom;
	@Column(name="departamentId")
	private int departamentId;
	@Column(name="dataAlta")
	private LocalDateTime dataAlta;
	
	@ManyToOne(cascade= {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name = "departamentId")
	private Departament departament;
	
	/**
	 * @param nom
	 * @param cognom
	 * @param departamentId
	 * @param dataAlta
	 */
	public Tripulant(String nom, String cognom, LocalDateTime dataAlta) {
		this.nom = nom;
		this.cognom = cognom;
		this.dataAlta = dataAlta;
	}

	/**
	 * Constructor sense arguments
	 */
	public Tripulant() {
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the cognom
	 */
	public String getCognom() {
		return cognom;
	}

	/**
	 * @param cognom the cognom to set
	 */
	public void setCognom(String cognom) {
		this.cognom = cognom;
	}

	/**
	 * @return the departamentId
	 */
	public int getDepartamentId() {
		return departamentId;
	}

	/**
	 * @param departamentId the departamentId to set
	 */
	public void setDepartamentId(int departamentId) {
		this.departamentId = departamentId;
	}

	/**
	 * @return the dataAlta
	 */
	public LocalDateTime getDataAlta() {
		return dataAlta;
	}

	/**
	 * @param dataAlta the dataAlta to set
	 */
	public void setDataAlta(LocalDateTime dataAlta) {
		this.dataAlta = dataAlta;
	}

	/**
	 * @return the departament
	 */
	public Departament getDepartament() {
		return departament;
	}

	/**
	 * @param departament the departament to set
	 */
	public void setDepartament(Departament departament) {
		this.departament = departament;
	}

	@Override
	public String toString() {
		return "Tripulant [id=" + id + ", nom=" + nom + ", cognom=" + cognom + ", departamentId=" + departamentId
				+ ", dataAlta=" + dataAlta + ", departament=" + departament + "]";
	}
	
}
