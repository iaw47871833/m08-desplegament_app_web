package curs_de_08_Acceso_a_datos;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ObtenirDadesDepartament {

	public static void main(String[] args) {
		// Creem l'objecte SessionFactory i li afegim les 2 classes amb les que volem
		// treballar
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Departament.class).addAnnotatedClass(DepartamentDadesExtres.class)
				.buildSessionFactory();
		System.out.println("Creem l'objecte Session.");
		Session elMeuSession = elMeuFactory.openSession();

		try {
			// Busquem totes les dades d'un departament fent servir la relació bidireccional
			// A partir d'un objecte Departament s'ha de trobar automàticament l'objecte
			// DepartamentDadesExtres associat i
			// a la inversa també
			System.out.println("Iniciem la transaccio");

			elMeuSession.beginTransaction();

			// Busquem Departament --> DepartamentDadesExtres:
			System.out.println("BUSQUEDA Departament --> DepartamentDadesExtres:");
			Departament departamentLlegitDeLaBD = elMeuSession.get(Departament.class, 1);

			if (departamentLlegitDeLaBD != null) {
				System.out.println("Registre llegit de la BD (departament): " + departamentLlegitDeLaBD.toString());
				System.out.println("Registre llegit de la BD (departament_dades_extres): "
						+ departamentLlegitDeLaBD.getDepartamentDadesExtres().toString());
			} else {
				System.out.println("No s'ha trobat cap departament amb id = 1.");
			}

			elMeuSession.getTransaction().commit();

			if (departamentLlegitDeLaBD != null) {
				System.out.println("Acabem la transacció. Exit amb el SELECT en la BD.");
			} else {
				System.out.println("Acabem la transacció. No s'ha fet cap SELECT en la BD.");
			}
			System.out.println();

			System.out.println("Iniciem la 2a transacció.");
			elMeuSession.beginTransaction();

			// Busqueda DepartamentDadesExtres --> Departament:
			System.out.println("BUSQUEDA DepartamentDadesExtres --> Departament:");
			DepartamentDadesExtres departamentDadesExtresLlegitDeLaBD = elMeuSession.get(DepartamentDadesExtres.class,
					2);

			if (departamentDadesExtresLlegitDeLaBD != null) {
				System.out.println("Registre llegit de la BD (departament_dades_extres): "
						+ departamentDadesExtresLlegitDeLaBD.toString());
				System.out.println("Registre llegit de la BD (departament): "
						+ departamentDadesExtresLlegitDeLaBD.getDepartament().toString());
			} else {
				System.out.println("No s'ha trobat cap departament amb id = 4");
			}

			elMeuSession.getTransaction().commit();

			if (departamentDadesExtresLlegitDeLaBD != null) {
				System.out.println("Acabem la transacció 2. Exit amb el SELECT en la BD.");
			} else {
				System.out.println("Acabem la transacció 2. No s'ha fet cap SELECT en la BD.");
			}
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}
	}

}
