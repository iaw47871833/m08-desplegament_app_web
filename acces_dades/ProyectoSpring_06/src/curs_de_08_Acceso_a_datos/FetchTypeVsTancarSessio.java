package curs_de_08_Acceso_a_datos;

import java.time.LocalDateTime;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class FetchTypeVsTancarSessio {

	public static void main(String[] args) {
		// Creem l'objecte SessionFactory i li afegim les 2 classes amb les que volem treballar
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml")
											.addAnnotatedClass(Departament.class)
											.addAnnotatedClass(DepartamentDadesExtres.class)
											.buildSessionFactory();
		System.out.println("Creem l'objecte Session.");
		Session elMeuSession = elMeuFactory.openSession();
		
		try {
			// Insertem un objecte de tipus Departament i com que aquest está connectat amb la classe DepartamentDadesExtres
			// amb la relació un a un (Annotations @OneToOne i @JoinColumn), també s'insertarà automàticament un objecte de
			// tipus DepartamentDadesExtres en la BD.
			System.out.println("Iniciem la transaccio");
			
			elMeuSession.beginTransaction();
			
			// Agafem de la BD el departament amb id = 3
			Departament departamentLlegitDelaBD = elMeuSession.get(Departament.class, 3);
			
			if (departamentLlegitDelaBD != null) {
				System.out.println("Departament: " + departamentLlegitDelaBD);
				System.out.println("Tripulants assignats: " + departamentLlegitDelaBD.getLlistaTripulants());
			} else {
				System.out.println("NO S'HA TROBAT CAP DEPARTAMENT");
			}
			
			elMeuSession.getTransaction().commit();
			
			elMeuSession.close();
			
			if (departamentLlegitDelaBD != null) {
				System.out.println("Tripulants assignats: " + departamentLlegitDelaBD.getLlistaTripulants());
			}
			
			System.out.println("Acabem la transacció. Registre INSERTAT en la BD.");
		} finally {			
			elMeuFactory.close();
		}
	}

}
