package curs_de_08_Acceso_a_datos;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HQLSelectPlanetes {

	public static void main(String[] args) {
		System.out.println("Creem l'objecte SessionFactory");
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Planetes.class).buildSessionFactory();
	
		System.out.println("Creem l'objecte Session.");
		Session elMeuSession = elMeuFactory.openSession();
		
		try {
			// Insertem 2 registres en la BD.
			System.out.println("Iniciem la transacció.");
			elMeuSession.beginTransaction();
			
			// Consulta dels planetes sense condicions.
			// Com que al fer una consulta no sabrem si ens retornarà 1 o més resultats (1 o més registres de la BD), hem de ficar el resultat de la consulta en una llista.
			String sentenciaHQL = "FROM Planetes";
			List<Planetes> llistaPlanetes = elMeuSession.createQuery(sentenciaHQL).getResultList();
			
			for (Planetes planetaTmp: llistaPlanetes) {
				System.out.println(planetaTmp);
			}
			System.out.println("|----------------------------|");
			
			// Consulta dels planetes amb condicions i fent servir un alias de la classe
			sentenciaHQL = "FROM Planetes p WHERE p.nom = 'Saturn'";
			System.out.println("Llançem la consulta '" + sentenciaHQL + "'.");
			llistaPlanetes = elMeuSession.createQuery(sentenciaHQL).getResultList();
			
			for (Planetes planetaTmp: llistaPlanetes) {
				System.out.println(planetaTmp);
			}
			
			// Consulta dels planetes amb condicionos + operador lògic.
			sentenciaHQL = "FROM Planetes p WHERE p.nom = 'Mart' OR p.descripcio LIKE '%gaseós%'";
			System.out.println("4: Llançem la consulta '" + sentenciaHQL + "'.");
			llistaPlanetes = elMeuSession.createQuery(sentenciaHQL).getResultList();
			
			for(Planetes planetaTmp : llistaPlanetes) {
				System.out.println(planetaTmp);
			}
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}


	}

}
