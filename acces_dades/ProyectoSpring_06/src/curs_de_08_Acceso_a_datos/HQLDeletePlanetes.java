package curs_de_08_Acceso_a_datos;

import org.hibernate.Session;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HQLDeletePlanetes {

	public static void main(String[] args) {
		System.out.println("Creem l'objecte SessionFactory");
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Planetes.class).buildSessionFactory();
	
		System.out.println("Creem l'objecte Session.");
		Session elMeuSession = elMeuFactory.openSession();
		
		try {
			// Insertem 2 registres en la BD.
			System.out.println("Iniciem la transacció.");
			elMeuSession.beginTransaction();
			
			System.out.println();
			
			String sentenciaHQL = "DELETE Planetes WHERE nom ='Jupiter'";
			System.out.println("1: Llançem la sentència '" + sentenciaHQL + "'.");
			elMeuSession.createQuery(sentenciaHQL).executeUpdate();
			
			elMeuSession.getTransaction().commit(); // Perquè s'executi la nova transacció.
			
			
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}

	}

}
