<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>formulariNomResposta.jsp</title>
	
	<!-- Carreguem un full d'estils css. -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/elMeuEstil.css">
</head>

<body>
	Video 28 (omplert el formulari del video 28): <br>
	Aquesta és la resposta que envia el server al formulari que ha enviat l'usuari. <br>
	El nom del tripulant enviat per l'usuari a través del formulari ha estat: <b> ${param.tripulantNom_28} </b>

	<br><br>
	----------------
	<br><br>
	
	Video 29 (omplert el formulari del video 29): <br>
	<b>missatgePerRetornar:</b> ${missatgePerRetornar_29}
	<!-- "${XXX}" és un tag de JSP. -->
	
	<br><br>
	----------------
	<br><br>
	
	Video 30: <br>
	<img alt="imatge d'un destructor" src="${pageContext.request.contextPath}/recursos/imatges/avenged_skull.jpg">
	<!-- El tag JSP "${pageContext.request.contextPath }" ens retorna l'arrel del nostre lloc web (.../WebContent). -->
	
	<br><br>
	----------------
	<br><br>
	
	Video 32 (omplert el formulari del video 32): <br>
	<b>missatgePerRetornar:</b> ${missatgePerRetornar_32}
	<!-- "${XXX}" és un tag de JSP. -->
	
		
	<br><br>
	----------------
	<br><br>
	
	Video 33 (omplert el formulari del video 33): <br>
	<b>missatgePerRetornar:</b> ${missatgePerRetornar_33}
	<!-- "${XXX}" és un tag de JSP. -->
	
</body>
</html>
