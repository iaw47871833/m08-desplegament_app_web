package curs_de_05_AplicacionesWeb;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/*
MVC = Model Vista Controlador (una interface amb la que interactua l'usuari, que serà una pàgina web, i una BD).
MVC és un patró de disseny de pàgines web que fan servir els frameworks.

El model MVC consisteix en dividir el nostre programa en 3 mòduls (la Vista, el Controlador i el Model).

Dividirem la nostra aplicació en 3 parts: Vista - Controlador - Model.
Vista: la interficie amb l'usuari (sera 1 pàgina web hmtl o jsp). Generalment es fa amb JSP (Java Server Pages).
Model: la BD o els recursos als que volen accedir els usuaris (contenidor de dades de l'aplicació que pot ser una BD, un fitxer txt / xml / json,...).
Controlador: qui té el codi per accedir a la BD o als recursos als que volen accedir els usuaris. Serà un Servlet.

L'usuari demana algo (select, update, delete,...) a través de la interficie (la Vista), a llavors la petició arriba al Controlador que serà qui 
envii la petició al Model. El Model (generalment una BD) li retorna les dades al Controlador i aquest les passa a la Vista.


Hem canviat la perspectiva d'Eclipse de Java --> Java EE (aixó està al menú d'icones a la dreta de la pantalla, al costat de la caixa "Quick Access").

Creem un projecte "Dynamic Web Project" per a crear pàgines web dinàmiques. Amb les opcions per defecte tenim prou. Ens hem d'asegurar que en 
l'apartat Target runtime tinguem sel·lecccionat <None> i sinó el sel·leccionem de la llista que hi ha.

Dins del projecte existeix la carpeta "WebContent" que és on anirà el contingut web (css, imatges, jsp's,...).

Dins de la carpeta WebContent/WEB_INF/lib hem de ficar la llibreria de Spring. 
Descomprimim el zip que té la llibreria de Spring --> sel·leccionem tots els fitxers jar que hi ha dins de la carpeta libs --> fem copy --> ens posem sobre la carpeta WebContent/WEB_INF/lib --> fem botó dret --> i a llavors fem paste.

El fitxer "video27_1.zip" conté 2 llibreries que també hem d'importar. Els sel·leccionem, fem copy, ens posem sobre la carpeta WebContent/WEB_INF/lib, 
fem botó dret i a llavors fem paste. 

El fitxer "video27_2.zip" conté 2 fitxers de configuració (2 xml). El descomprimim i copiem els 2 fitxers en WebContent/WEB_INF. Més endavant farem servir codi java 
per a fer la configuració del projecte en comptes de fitxers xml.   
A partir d'aquest 2 fitxers XML es pot configurar qualsevol projecte que fem.
ALERTA: al copiar els 2 XML, els hem d'editar perquè 3 paraules amb accent s'han descojonat i surt 1 caràcter raro en comptes d'1 lletra amb accent.

El codi de les vistes estaran dins de WebContent/WEB-INF/vistes (és lo que hem ficat en el fitxer de configuració spring_mvc_servlet.xml).
El codi dels controladors estaran dins del paquet que hem creat dins de Java Resources (el típic paquet java).

Per executar el projecte: anem al nom del projecte --> btn dret --> Run As --> Run on Server --> sel·leccionem el server que volem (nosaltres només tenim el Tomcat 9) -->
 --> Finish --> ara ha d'arrancar el servidor Tomcat (triga temps) i a llavors Eclipse obrirà un visor de pàgines web en una pestanya al costat de les pestanyes dels fitxers  
 de programació que estem fent.
 */


@Controller						// Aquesta Java Annotation determinarà que aquesta classe fa de controlador dins del model Model-Vista-Controlador. 
public class Controlador {		// Spring llegeix totes les classes amb @Controller ja que és on estan les URL's que es fan servir per a cridar a 
								// les funcions que seran les que retornin la vista que volem.
											
	@RequestMapping							// Aquesta javaAnnotation determina quina URL ha de fer servir l'usuari per a demanar la vista "vistaExemple_1.jsp". 
	public String mostrarVista1() {			// Com que no posa res, equival a @RequestMapping("/") que vol dir que serà l'arrel del nostre 
											// projecte, és a dir, que serà la vista que es carregarà per defecte si no s'especifica cap altre
											// en el paràmetre de @RequestMapping.
		return "vistaExemple_1";			// Amb aquesta java Annotation estem determinant que el mètode mostrarVista1() ha de rastrejar pel nostre
	}										// projecte per a trobar una vista anomenada vistaExemple_1.jsp (aquest fitxer jsp ha d'estar dins de WEB-INF/vistes).
											// 
											// Per a crear una vista: sobre la carpeta WEB-INF/vistes --> btn dret --> New --> JSP File (no posem extensió al fitxer).
}

