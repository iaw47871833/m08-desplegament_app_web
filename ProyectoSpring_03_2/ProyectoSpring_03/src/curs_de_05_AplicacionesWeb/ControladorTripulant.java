package curs_de_05_AplicacionesWeb;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping('/tripulant')
public class ControladorTripulant {
	@RequestMapping("/mostrarFormulariAltaTripulant")
	public String mostrarFormulari(Model model) {
		Tripulant doctor = new Tripulant();
		model.addAttribute("el_doctor", doctor);
		return "formulariAltaTripulant";
	}
	
	@RequestMapping("/procesarAltaTripulant")
	public String procesarFormulari(@ModelAttribute("el_doctor") Tripulant elNouTripulant) {
		if (resultatValidacio.hasErrors()) {
			return "formulariAltaTripulant";
		} else {
			return "confirmacioAltaNouTripulant";
		}
	}
}
