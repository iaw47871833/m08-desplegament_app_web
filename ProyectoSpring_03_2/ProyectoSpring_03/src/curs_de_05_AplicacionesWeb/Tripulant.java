package curs_de_05_AplicacionesWeb;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Tripulant {
	
	@NotNull
	@Size(min = 1, message = "Long. mínima de nom ha de ser 1 caràcter.")
	private String nom;
	private String cognom;
	@NotNull
	@Min(value = 18, message = "El nou tripulant ha de tenir com a mínim a 18 anys.")
	@Max(value = 65, message = "El nou tripulant ha de tenir com a màxim 65 anys.")
	private int edat;
	public int getEdat() {
		return edat;
	}
	public void setEdat(int edat) {
		this.edat = edat;
	}
	@NotBlank(message = "S'ha d'omplir l'email.")
	@Email
	private String email;
	private String departament;
	private String conneixements;
	private String ciutatNaixement;
	private String idiomes;
	
	public String getCiutatNaixement() {
		return ciutatNaixement;
	}
	public void setCiutatNaixement(String ciutatNaixement) {
		this.ciutatNaixement = ciutatNaixement;
	}
	public String getIdiomes() {
		return idiomes;
	}
	public void setIdiomes(String idiomes) {
		this.idiomes = idiomes;
	}
	public String getConneixements() {
		return conneixements;
	}
	public void setConneixements(String conneixements) {
		this.conneixements = conneixements;
	}
	public String getNom() {
		return nom;
	}
	public String getDepartament() {
		return departament;
	}
	public void setDepartament(String departament) {
		this.departament = departament;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getCognom() {
		return cognom;
	}
	public void setCognom(String cognom) {
		this.cognom = cognom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}
