package curs_de_04_JavaAnnotations;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

@Component("controladorSondesNau")
public class ControladorSondes implements Tripulants {
	
	private String nom;
	
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	public ControladorSondes() {
		
	}

	@PostConstruct
	public void inicialitzador() {
		this.setNom("hola");
		System.out.println("Nom controlador sondes: " + this.getNom());
		System.out.println("ControladorSondes: S'HA EXECUTAT inicialitzador() AMB JAVA ANNOTATION @PostConstruct.");
	}
	
	@PreDestroy
	public void finalitzador() {
		this.setNom("adeu");
		System.out.println("Nom controlador sondes: " + this.getNom());
		System.out.println("ControladorSondes: S'HA EXECUTAT finalitzador() AMB LA JAVA ANNOTATION @PreDestroy.");
	}
	
	@Override
	public String agafarTarees() {
		return "controladorSondesNau: agafarTarees()";
	}

	@Override
	public String agafarInforme() {
		return "controladorSondesNau: agafarInforme()";
	}

}
