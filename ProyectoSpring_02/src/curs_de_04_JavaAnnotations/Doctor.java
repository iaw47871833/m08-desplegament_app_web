/**
 * 
 */
package curs_de_04_JavaAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author iaw47871833
 *
 */

/*
 * Creem una java annotation que servirà per a crear un bean (generador d'objectes) de la classe Doctor.class sense haver-lo
 * de crear en el fitxer XML de configuració.
 * Entre parèntesis el ID quie volem que tingui el bean amb el que crearem objectes de la classe Doctor.java. Si no fiquem el ID
 * a llavors el ID del bean serà automàticament el nom de la classe però amb la 1a lletra en minúscula (seria "doctor").
 */
@Component("doctorNau")
public class Doctor implements Tripulants {
	
	// Atribut
	private InformeInfermeriaInterface informeDoctor;
	
	/**
	 * @param informeDoctor
	 */
	// Apliquem la injecció de dependències automatica
	@Autowired
	public Doctor(InformeInfermeriaInterface informeDoctor) {
		this.informeDoctor = informeDoctor;
	}

	@Override
	public String agafarTarees() {	
		return "doctorNau: agafarTarees(): curar als tripulants. ";
	}

	@Override
	public String agafarInforme() {
		return "doctorNau: agafarInforme(): Informe del doctor de la missió. " + informeDoctor.getInformeInfermeria();
	}

}
