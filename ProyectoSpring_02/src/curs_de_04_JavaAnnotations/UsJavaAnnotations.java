package curs_de_04_JavaAnnotations;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsJavaAnnotations {

	public static void main(String[] args) {
		System.out.println("------- UsJavaAnnotations - INICI -------");
		System.out.println();
		
		// Creem el contexte carregant el fitxer xml de configuració.
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext.xml");

		// Demanem el bean al contenidor fent servir la java annotation.
		Tripulants doctor = contexte.getBean("doctorNau", Doctor.class);
		
		// Fem servir el bean
		System.out.println(doctor.agafarTarees());
		System.out.println(doctor.agafarInforme());
		System.out.println("-----------");
		
		// Tanquem el contexte
		contexte.close();
		
		System.out.println();
		System.out.println("------------ UsJavaAnnotations - FI ------------");
	}

}
