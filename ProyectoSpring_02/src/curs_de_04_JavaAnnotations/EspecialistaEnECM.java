package curs_de_04_JavaAnnotations;

import org.springframework.beans.factory.annotation.Value;

public class EspecialistaEnECM implements Tripulants {

	// Atributs
	private InformeElectronicaInterface informeEspecialistaEnECM;
	
	private InformeElectronicaInterface informeEspecialistaEnESM;
	private InformeElectronicaInterface informeEspecialistaEnEPM;
	
	
	@Value("${emailDepartamentECM}") // Aixi injectem el vlaor de la propietat emailDepartamentECM (del fitxer properties)
	private String email;			 // en l'atribut "email" (col·locant la Annotation @Value sobre l'atribut)
	
	@Value("${departamentECMNom}")
	private String departamentNom;
	
	/**
	 * @param informeEspecialistaEnECM
	 * @param informeEspecialistaEnESM
	 * @param informeEspecialistaEnEPM
	 */
	public EspecialistaEnECM(InformeElectronicaInterface informeEspecialistaEnECM,
			InformeElectronicaInterface informeEspecialistaEnESM,
			InformeElectronicaInterface informeEspecialistaEnEPM) {
		this.informeEspecialistaEnECM = informeEspecialistaEnECM;
		this.informeEspecialistaEnESM = informeEspecialistaEnESM;
		this.informeEspecialistaEnEPM = informeEspecialistaEnEPM;
	}

	/**
	 * @param informeEspecialistaEnECM
	 */
	public EspecialistaEnECM(InformeElectronicaInterface informeEspecialistaEnECM) {
		this.informeEspecialistaEnECM = informeEspecialistaEnECM;
	}
	
	public String imprimirTripleInforme() {
		String cadena = "EspecialistaEnECM: imprimirTripleInforme(): \n \t" +
				"Informe ECM: " + informeEspecialistaEnECM.getInformeElectronica() + "\n\t" +
				"Informe ESM: " + informeEspecialistaEnESM.getInformeElectronica() + "\n \t" +
				"Informe EPM: " + informeEspecialistaEnEPM.getInformeElectronica() + "\n \t";
		return cadena;
	}
	

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the departamentNom
	 */
	public String getDepartamentNom() {
		return departamentNom;
	}

	/**
	 * @param departamentNom the departamentNom to set
	 */
	public void setDepartamentNom(String departamentNom) {
		this.departamentNom = departamentNom;
	}

	@Override
	public String agafarTarees() {
		return "EspecialistaEnECM: agafarTarees(): arreglar la electrònica d'Infermeria.";
	}

	@Override
	public String agafarInforme() {
		return informeEspecialistaEnECM.getInformeElectronica();
	}

}
