package curs_de_04_JavaAnnotations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration 	// Amb aquesta anotation indiquem a Spring que aquest serà el fitxer de configuració de
				// la nostra aplicació substituint al fitxer xml (el applicationContext.xml)

@ComponentScan("curs_de_04_JavaAnnotations")

@PropertySource("classpath:dadesNau.properties")

public class aplicacioConfig {
	// Definim el bean per a InformeECM.java
	// L'identificador d'aquest bean serà el nom del métode (informeECM()). Podem posar el nom que volguem al mètode.
	@Bean
	public InformeElectronicaInterface informeECM() {
		return new InformeECM();
	}
	
	@Bean
	public InformeElectronicaInterface informeESM() {
		return new InformeESM();
	}
	
	@Bean
	public InformeElectronicaInterface informeEPM() {
		return new InformeEPM();
	}
	
	
	// Definim el bean per a EspecialistaEnECM.java i a més injectem les dependencies (l'objecte InformeElectronicaInterface).
	@Bean
	public Tripulants especialistaEnECM() {
		return new EspecialistaEnECM(informeECM());
	}
	
	@Bean
	public Tripulants especialistaEnECMTripleInforme() {
		return new EspecialistaEnECM(informeECM(), informeESM(), informeEPM());
	}
}
