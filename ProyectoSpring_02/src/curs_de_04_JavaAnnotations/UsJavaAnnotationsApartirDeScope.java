package curs_de_04_JavaAnnotations;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsJavaAnnotationsApartirDeScope {

	public static void main(String[] args) {
		System.out.println("------------ usJavaAnnotationsApartirDeScope - INICI -------------");
		System.out.println();
		
		// Creació del contexte
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		// Scope - INICI
		
		// Peticio de beans al contenidor Spring
		Tripulants doctor_1 = contexte.getBean("doctorNau", Doctor.class);
		Tripulants doctor_2 = contexte.getBean("doctorNau", Doctor.class);
		
		// Veurem que la direcció de memòria és la mateixa i per tant són el mateix objecte.
		System.out.println("Patró SINGLETON:");
		System.out.println("Dir. de memòria de doctor_1: " + doctor_1);
		System.out.println("Dir. de memòria de doctor_2: " + doctor_2);
		
		// Peticio de beans al contenidor Spring
		Tripulants infermera_1 = contexte.getBean("infermeraNau", Infermera.class);
		Tripulants infermera_2 = contexte.getBean("infermeraNau", Infermera.class);
		
		// Veurem que la direcció de memòria és diferent i per tant són diferents objectes.
		System.out.println("Patró PROTOTYPE:");
		System.out.println("Dir. de memòria de infermera_1: " + infermera_1);
		System.out.println("Dir. de memòria de infermera_2: " + infermera_2);
		
		// Scope - FI
		
		// Tanquem el contexte
		contexte.close();
		System.out.println("-------------------------------------");
		
		System.out.println("ABANS DE CARREGAR EL CONTEXT");
		
		//ClassPathXmlApplicationContext contexte2 = new ClassPathXmlApplicationContext("applicationContext.xml");
		AnnotationConfigApplicationContext contexte2 = new AnnotationConfigApplicationContext(aplicacioConfig.class);
		
		System.out.println("DESPRÉS DE CARREGAR EL CONTEXT I ABANS DE CARREGAR EL BEAN");
		
		ControladorSondes controladorSondes = contexte2.getBean("controladorSondesNau", ControladorSondes.class);
		
		System.out.println("DESPRÉS DE CARREGAR EL BEAN");
		
		System.out.println(controladorSondes.agafarInforme());
		
		System.out.println("DESPRÉS D'EXECUTAR controladorSondes.agafarInforme()");
		
		System.out.println(controladorSondes.agafarTarees());
		
		System.out.println("DESPRÉS D'EXECUTAR controladorSondes.agafarTarees()");
		
		// Tanquem el contexte
		contexte2.close();
		
		System.out.println("DESPRÉS DE TANCAR EL CONTEXT");
		
		// Creem un contexte a partir de la class de configuració (que serà el que substituirà al xml)
		
		AnnotationConfigApplicationContext contexte3 = new AnnotationConfigApplicationContext(aplicacioConfig.class);
		Tripulants especialistaEnECM_1 = contexte3.getBean("especialistaEnECM", EspecialistaEnECM.class);
		
		System.out.println("Tripulants especialistaEnECM.agafarInforme(): " + especialistaEnECM_1.agafarInforme());
		System.out.println("Tripulants especialistaEnECM.agafarTarees(): " + especialistaEnECM_1.agafarTarees());
		
		EspecialistaEnECM especialistaEnECM_2 = contexte3.getBean("especialistaEnECM", EspecialistaEnECM.class);
		
		System.out.println("EspecialistaEnECM especialistaEnECM.agafarTarees(): " + especialistaEnECM_2.agafarTarees());
		System.out.println("EspecialistaEnECM especialistaEnECM.getEmail(): " + especialistaEnECM_2.getEmail());
		System.out.println("EspecialistaEnECM especialistaEnECM.getDepartamentNom(): " + especialistaEnECM_2.getDepartamentNom());
		
		Tripulants especialistaEnECM_10 = contexte3.getBean("especialistaEcECM", EspecialistaEnECM.class);
		
		System.out.println("Tripulants especialistaEnECM.agafarInforme: " + especialistaEnECM_10.agafarInforme());
		System.out.println("Tripulants especialistaEnECM.agafatTarees(): " + especialistaEnECM_10.agafarTarees());
		
		EspecialistaEnECM especialistaEnECM_20 = contexte3.getBean("especialistaEnECMTripleInforme", EspecialistaEnECM.class);
		
		System.out.println("EspecialistaEnECM especialistaEnECMTripleInforme.imprimirTripleInforme(): " + especialistaEnECM_20.imprimirTripleInforme());
		System.out.println("EspecialistaEnECM especialistaEnECMTripleInforme.agafarTarees(): " + especialistaEnECM_20.agafarTarees());
		System.out.println("EspecialistaEnECM especialistaEnECMTripleInforme.getEmail(): " + especialistaEnECM_20.getEmail());
		System.out.println("EspecialistaEnECM especialistaEnECMTripleInforme.getDepartamentNom(): " + especialistaEnECM_20.getDepartamentNom());
		
		
		contexte3.close();
		
		System.out.println("------------ usJavaAnnotationsApartirDeScope - FI -------------");
	}

}
