package curs_de_04_JavaAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("infermeraNau") // Per a crear un bean(generador d'objectes) de la classe Infermera.class sense haver-lo
							// de crear en el fitxer XML de configuració
@Scope("prototype")			// Ara cada vegada que creem un bean d'aquesta classe, es crearà un objecte diferent
public class Infermera implements Tripulants {

	// Atributs
	private InformeInfermeriaInterface informeInfermera;
	// Podem fer:
	// @Autowired
	// private InformeInfermeriaInterface informeInfermera;
	@Autowired
	@Qualifier("mantenimentInfermeriaMaterial")
	private MantenimentInfermeriaInterface mantenimentMaterial;
	@Qualifier("mantInferMedica")
	@Autowired
	private MantenimentInfermeriaInterface mantenimentMedicaments;
	
	@Override
	public String agafarTarees() {
		return "infermeraNau: agafarTarees(): ajudar a curar als tripulants. ";
	}

	@Override
	public String agafarInforme() {
		return "infermeraNau: agafarInforme(): Informe de l'infermera de la missió. " + informeInfermera.getInformeInfermeria();
	}
	
	// Com que el setter porta el @Autowired, el setter s'executarà automàticament quan cridem al bean d'aquesta classe
	@Autowired
	public void setInformeInfermera(InformeInfermeriaInterface informe) {
		this.informeInfermera = informe;
	}
	
	// Injecció (d'un objecte de tipus InformeDoctor.java) amb un mètode.
	// Com que el mètode porta el @Autowired, el mètode s'executarà automàticament quan cridem al bean d'aquesta classe.
	@Autowired
	public void injectarInformeEnInfermera(InformeInfermeriaInterface informe) {
		this.informeInfermera = informe;
	}

}
