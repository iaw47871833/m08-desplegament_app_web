/**
 * 
 */
package curs_de_04_JavaAnnotations;

import org.springframework.stereotype.Component;

/**
 * @author iaw47871833
 *
 */

/*
 * Fem servir la Java Annotation que servirà per a crear un bean de la classe InformeDctor.class sense
 * haver-lo de crear en el XML.
 * Com que no li hem posat cap paràmetre a @Component a llavors per defecte posarà el nom de la classe
 * però començant amb minúscula, és a dir, és com si haguessim fet: @Component("informeDoctor"). 
 */
@Component
public class InformeDoctor implements InformeInfermeriaInterface {

	@Override
	public String getInformeInfermeria() {
		return "Informe d'infermeria generat pel doctor (generat per InformeDoctor.java).";
	}

}
