<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>formulariNom.jsp</title>
</head>
<body>
	Formulari del video 28:
	<form action="respostaDelFormulari_28" method="get">
		<!-- En action hem de posar la URL de la pàgina web que rebra el formulari i el procesarà. Ha de coincidir amb algun dels @RequestMapping("/xxx") de FormulariWebControlador.java -->
		Nom del tripulant: <input type="text" name="tripulantNom_28">
		<br>
		<input type="submit"> 
	</form>
	
	<br>
	<br>
	<br>
	
	Formulari del video 29:
	<form action="respostaDelFormulari_29" method="get">
		<!-- En action hem de posar la URL de la pàgina web que rebra el formulari i el procesarà. Ha de coincidir amb algun dels @RequestMapping("/xxx") de FormulariWebControlador.java -->
		Nom del tripulant: <input type="text" name="tripulantNom_29">
		<br>
		<input type="submit"> 
	</form>
	
	<br>
	<br>
	<br>
	
	Formulari del video 32:
	<form action="respostaDelFormulari_32" method="get">
		<!-- En action hem de posar la URL de la pàgina web que rebra el formulari i el procesarà. Ha de coincidir amb algun dels @RequestMapping("/xxx") de FormulariWebControlador.java -->
		Nom del tripulant: <input type="text" name="tripulantNom_32">
		<br>
		<input type="submit"> 
	</form>
	
	<br>
	<br>
	<br>
	
	Formulari del video 33:
	<form action="dirSegon/respostaDelFormulari_29" method="get">
		<!-- En action hem de posar la URL de la pàgina web que rebra el formulari i el procesarà. Ha de coincidir amb algun dels @RequestMapping("/xxx") de FormulariWebControlador.java -->
		Nom del tripulant: <input type="text" name="tripulantNom_33">
		<br>
		<input type="submit"> 
	</form>
	
</body>
</html>
