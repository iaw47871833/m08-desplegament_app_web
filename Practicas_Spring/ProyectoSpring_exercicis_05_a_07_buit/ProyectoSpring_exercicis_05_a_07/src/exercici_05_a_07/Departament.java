package exercici_05_a_07;

import java.util.ArrayList;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class Departament {
	private int id;
	private String nom;
	private String email;
	private Organitzacio organitzacio;
	private Personal capDeDepartament;
	private ArrayList<Personal> llistaPersonal;
	
	/**
	 * @param id
	 * @param nom
	 * @param email
	 * @param organitzacio
	 * @param capDeDepartament
	 * @param llistaPersonal
	 */
//	public Departament(int id, String nom, String email, Organitzacio organitzacio, Personal capDeDepartament,
//			ArrayList<Personal> llistaPersonal) {
//		this.id = id;
//		this.nom = nom;
//		this.email = email;
//		this.organitzacio = organitzacio;
//		this.capDeDepartament = capDeDepartament;
//		this.llistaPersonal = llistaPersonal;
//	}
//	

	/**
	 * @param id
	 * @param nom
	 * @param email
	 */
//	public Departament(int id, String nom, String email) {
//		super();
//		this.id = id;
//		this.nom = nom;
//		this.email = email;
//	}
	
	/**
	 * @param id
	 */
	public Departament(int id) {
		this.id = id;
	}


	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the organitzacio
	 */
	public Organitzacio getOrganitzacio() {
		return organitzacio;
	}

	/**
	 * @param organitzacio the organitzacio to set
	 */
	public void setOrganitzacio(Organitzacio organitzacio) {
		this.organitzacio = organitzacio;
	}

	/**
	 * @return the capDeDepartament
	 */
	public Personal getCapDeDepartament() {
		return capDeDepartament;
	}

	/**
	 * @param capDeDepartament the capDeDepartament to set
	 */
	public void setCapDeDepartament(Personal capDeDepartament) {
		this.capDeDepartament = capDeDepartament;
	}

	/**
	 * @return the llistaPersonal
	 */
	public ArrayList<Personal> getLlistaPersonal() {
		return llistaPersonal;
	}

	/**
	 * @param llistaPersonal the llistaPersonal to set
	 */
	public void setLlistaPersonal(ArrayList<Personal> llistaPersonal) {
		this.llistaPersonal = llistaPersonal;
	}
}
