package exercici_05_a_07;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes({"personal"})
public class Controlador {

	private List<Personal> llistaPersonal;
	private List<Departament> llistaDepartaments = new ArrayList<Departament>();

	private boolean usuariExisteix(String dni) {
		for (Personal p : this.llistaPersonal) {
			if (p.getDni().equals(dni)) {
				return true;
			}
		}
		return false;
	}

	private boolean contrasenyaValida(String dni, String contrasenya) {
		for (Personal p : this.llistaPersonal) {
			if (p.getDni().equals(dni) && p.getContrasenya().equals(contrasenya)) {
				return true;
			}
		}
		return false;
	}
	
	/*
	 * private boolean loginValid(String dni, String contrasenya) { for (Personal p:
	 * this.llistaPersonal) { if (p.getDni().equals(dni) &&
	 * p.getContrasenya().equals(contrasenya)) { return true; } } return false; }
	 */

	@RequestMapping("/")
	public String inici() {
		this.llistaPersonal = usAplicacio.inicialitzarPersonals();
		this.llistaDepartaments = usAplicacio.inicialitzarDepartaments();
		System.out.println("Llista personal");
		for (Personal p : llistaPersonal) {
			System.out.println(p.getNom());
			System.out.println("dni: " + p.getDni());
			System.out.println(p.getContrasenya());
		}
		
		for (Departament d : llistaDepartaments) {
			System.out.println(d.getNom());
		}
		return "inici";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model) {
		Personal p = new Personal();
		model.addAttribute("personal", p);
		return "login";
	}

	@RequestMapping(value = "/loginCheck", method = RequestMethod.POST)
	public String loginCheck(HttpServletRequest request, Model model) {
		// Comprobem si existeix usuari i contrasenya
		Personal p = new Personal();
		String usuariDni = request.getParameter("usuari-dni");
		String usuariContrasenya = request.getParameter("usuari-contrasenya");
		System.out.println("usuariDni: " + usuariDni);
		if (!usuariExisteix(usuariDni)) {
			System.out.println("no existeix");
			model.addAttribute("error", String.format("L'usuari '%s' no existeix.", usuariDni));

		} else if (usuariExisteix(usuariDni) && !contrasenyaValida(usuariDni, usuariContrasenya)) {
			model.addAttribute("error", String.format("La contrasenya de l'usuari '%s' no es vàlida.", usuariDni));

		} else {
			for (Personal pers: this.llistaPersonal) {
				if (pers.getDni().equals(usuariDni) && pers.getContrasenya().equals(usuariContrasenya)) {
					p = pers;
					break;
				} 
			}
			model.addAttribute("personal", p);
			model.addAttribute("departaments", this.llistaDepartaments);
			return "modificarDadesUsuari";
		}
		for (Personal per : llistaPersonal) {
			System.out.println(per.getNom());
		}
		return "login";
	}

	@RequestMapping("/procesarLogin")
	public String procesarFormulari(@ModelAttribute("personal") Personal elNouPersonal) {
		return "confirmacioAltaNouPersonal";
	}

	@RequestMapping(value = "/procesarAltaPersonal", method = RequestMethod.POST)
	public String procesarFormulariPersonal(HttpServletRequest request, Model model) {
		// Comprobem si existeix l'usuari
		String personalDni = request.getParameter("dniPersonal");
		String personalNom = request.getParameter("nomPersonal");
		String personalContrasenya = request.getParameter("contrasenyaPersonal");
		if (usuariExisteix(personalDni)) {
			model.addAttribute("error", String.format("L'usuari amb dni '%s' ja existeix.", personalDni));
			return "donarAltaPersonal";
		}
		// Si no existeix creem l'usuari
		Personal p = new Personal(personalDni, personalNom, personalContrasenya, LocalDate.now().minusMonths(1), 1);
		// L'afegim a la llista
		this.llistaPersonal.add(p);
		model.addAttribute("success", "Usuari creat correctament");
		// Retornem la vista de login
		model.addAttribute("personal", p);
		return "login";
	}

	@RequestMapping("/registre")
	public String registre(Model model) {
		Personal p = new Personal();
		model.addAttribute("personal", p);
		return "donarAltaPersonal";
	}
	
	@RequestMapping("/modificarDadesUsuari")
	public String modificarUsuari(Model model, @ModelAttribute("personal") Personal p) {
		model.addAttribute("personal", p);
		model.addAttribute("departaments", this.llistaDepartaments);
		for (Departament d : this.llistaDepartaments) {
			System.out.println("departament " + d.getNom());
		}
		return "modificarDadesUsuari";
	}
	
	@RequestMapping(value= "/mostrarDadesUsuari", method = RequestMethod.POST)
	public String mostrarUsuari(@ModelAttribute("personal") @Valid Personal personal, BindingResult validacio, Model model, HttpServletRequest request) {
		Personal p = new Personal();
		String usuariDni = request.getParameter("dni");
		String usuariNom = request.getParameter("nom");
		String usuariCognom = request.getParameter("cognom");
		String usuariEmail = request.getParameter("email");
		String usuariEdat = request.getParameter("edat");
		String usuariContrasenya = request.getParameter("contrasenya");
		String usuariDepartament = request.getParameter("departament");
		System.out.println("el nomes: " + usuariNom);
		System.out.println("la contrasenya es: " + usuariContrasenya);
		System.out.println("la departament es: " + usuariDepartament);
		
		for (Personal pers: this.llistaPersonal) {
			if (pers.getDni().equals(usuariDni)) {
				p = pers;
				System.out.println("trobat");
				break;
			} 
		}
		model.addAttribute("personal", p);
		model.addAttribute("departaments", this.llistaDepartaments);
		
		if (validacio.hasErrors()) {
			return "modificarDadesUsuari";
		}
		// Modifiquem les dades
		p.setNom(usuariNom);
		p.setCognom(usuariCognom);
		p.setEmail(usuariEmail);
		p.setEdat(Integer.parseInt(usuariEdat));
		p.setDni(usuariDni);
		p.setContrasenya(usuariContrasenya);
		p.setDepartament(Integer.parseInt(usuariDepartament));
		return "mostrarDadesUsuari";
	}
	
	public void retallarEspaisEnBlanc(WebDataBinder binder) {
		StringTrimmerEditor objRetalladorEspaisEnBlanc = new StringTrimmerEditor(true);
		binder.registerCustomEditor(String.class, objRetalladorEspaisEnBlanc);
	}

	/*
	 * public String registreCheck(HttpServletRequest request, Model model) {
	 * //inicialitzaModel(model); String usuariDni =
	 * request.getParameter("usuari-dni"); String usuariNom =
	 * request.getParameter("usuari-nom"); String usuariContrasenya =
	 * request.getParameter("usuari-contrasenya"); String usuariDepartament =
	 * request.getParameter("usuari-departament");
	 * 
	 * if (usuariExisteix(usuariDni)) { model.addAttribute("error",
	 * "L'usuari ja existeix a la base de dades..."); return "donarAltaUsuari"; } //
	 * Tot OK!
	 * 
	 * Personal p = new Personal(); Departament d = (Departament)
	 * obtenirBean(usuariDepartament);
	 * 
	 * p.setDni(usuariDni); p.setNom(usuariNom);
	 * p.setContrasenya(usuariContrasenya); p.setDepartament(d.getId());
	 * this.llistaPersonal.add(p);
	 * 
	 * d.getLlistaPersonal().add(p); model.addAttribute("exit",
	 * String.format("Usuari amb DNI '%s' creat amb èxit.", usuariDni));
	 * 
	 * return "login"; }
	 */

	private Object obtenirBean(String nomBean) {
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(aplicacioConfig.class);
		Object valorBean = ctx.getBean(nomBean);
		ctx.close();
		return valorBean;
	}

}
