package exercici_05_a_07;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component // Java Annotation perquè es crei un bean d'aquesta classe automàticament.
public class ServiceClassMultiparametres2_v2 {
	private final BeanFactory factory;
	private List<Personal> llistaPersonal;
	private List<Departament> llistaDepartaments;

	/*
	 * public ServiceClassMultiparametres2_v2(BeanFactory factory, List<Departament>
	 * protoTypeBeanMultiparametres) { this.factory = factory;
	 * this.llistaDepartamentsMultiparametres = protoTypeBeanMultiparametres; }
	 */

	@Autowired
	public ServiceClassMultiparametres2_v2(final BeanFactory f) {
		this.factory = f;
	}
	
	public void creacioPersonal(List<String> llistaDniPersonals, List<String> llistaNomsPersonals, 
			List<Integer> llistaIdsDepartament, List<String> llistaContrasenyaPersonals, List<LocalDate> llistaDataCreacioPersonals) {
			this.llistaPersonal = llistaDniPersonals.stream().map(param -> factory.getBean(Personal.class, param)).collect(Collectors.toList());
	
			int index = 0;
			
			for (Personal personal: this.llistaPersonal) {
				personal.setNom(llistaNomsPersonals.get(index));
				personal.setDataCreacio(llistaDataCreacioPersonals.get(index));
				personal.setDepartament(llistaIdsDepartament.get(index));
				personal.setContrasenya(llistaContrasenyaPersonals.get(index));
				index++;
			}
	}
	
	public void creacioDepartament(List<Integer> llistaIdDepartaments, List<String> llistaNomDepartaments, List<String> llistaEmailDepartaments) {
		this.llistaDepartaments = llistaIdDepartaments.stream().map(param -> factory.getBean(Departament.class, param)).collect(Collectors.toList());
		
		int index = 0;
		
		for (Departament dept: this.llistaDepartaments) {
			dept.setId(llistaIdDepartaments.get(index));
			dept.setNom(llistaNomDepartaments.get(index));
			dept.setEmail(llistaEmailDepartaments.get(index));
			index++;
		}
	}

	public void demoMethod_v2(List<Integer> llistaIdsDepartament, List<String> llistaNomsDepartament,
			List<String> llistaEmailsDepartament) {
		// Various versions of getBean() method return an instance of the specified
		// bean.

		// Recorre tota la llista 'llistaIdsDepartament' a saco creant un flux amd les
		// dades que conté (stream()) i
		// per a cada element (param) aplica la funció que li passem dins del map() de
		// manera que ens retorna
		// un flux amb tots els resultats (un flux amb els objectes de tipus Departament
		// creats amb
		// 'factory.getBean(Departament.class, param)' i inicialitzats segons el
		// contructor Departament.public Departament(int id){}).
		//
		// Converteix el flux resultant en una llista gràcies al
		// collect(Collectors.toList()).
		// El collect() és el que canviarà el flux de dades en un altre cosa que serà
		// una llista gràcies al paràmetre 'Collectors.toList()'.

		this.llistaDepartaments = llistaIdsDepartament.stream()
				.map(param -> factory.getBean(Departament.class, param)).collect(Collectors.toList());

		int index = 0;
		for (Departament dept : this.llistaDepartaments) {
			dept.setNom(llistaNomsDepartament.get(index));
			dept.setEmail(llistaEmailsDepartament.get(index));

			index++;
		}
	}

	public List<Personal> getPersonal() {
		return llistaPersonal;
	}
	
	public List<Departament> getLlistaDepartaments() {
		return llistaDepartaments;
	}
}