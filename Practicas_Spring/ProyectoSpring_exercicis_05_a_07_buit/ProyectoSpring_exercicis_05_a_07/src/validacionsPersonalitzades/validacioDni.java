package validacionsPersonalitzades;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;
import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = validarDni.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface validacioDni {
	// String per fer les validacions
	public String value() default "TRWAGMYFPDXBNJZSQVHLCKE";
	String message() default "El format del DNI no es valido.";
	
	Class<?>[] groups() default {};
	
	Class<? extends Payload>[] payload() default {};

}
