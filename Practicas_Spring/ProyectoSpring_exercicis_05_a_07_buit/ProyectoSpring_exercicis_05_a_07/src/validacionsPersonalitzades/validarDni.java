package validacionsPersonalitzades;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class validarDni implements ConstraintValidator<validacioDni, String> {

	private String dniValid;
	
	@Override
	public void initialize(validacioDni annotationDniVal) {
		dniValid = annotationDniVal.value();
	}
	
	@Override
	public boolean isValid(String arg0, ConstraintValidatorContext arg1) {
		boolean resultat = false;
		if(arg0 != null) {
			int numeroDni = 0;
			String lletraDni = "", lletraValid = ""; 
			try {
				// Calculem la posicio de la lletra amb els numeros del dni
				numeroDni = Integer.parseInt(arg0.substring(0,8)) % 23;
				// Lletra del dni
				lletraDni = arg0.substring(8);
				lletraValid = "" + dniValid.charAt(numeroDni);
			} catch (Exception e) {
				System.out.println(e);
			}
			
			// Si son iguals el dni es valid
			if( lletraDni.equals(lletraValid) ) {
				resultat = true;
			}
		} 
		return resultat;

	}

}
