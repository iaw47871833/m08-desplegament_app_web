<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Login</title>
</head>
<body>
	<p>login.jsp</p>
	<form:form action="loginCheck" method="POST" modelAttribute="personal">
		<p style="color: red;">${error}</p>
		<p style="color: green;">${success}</p>
		<label for="usuari-dni">
		DNI usuari:
		</label><input type="text" name="usuari-dni"/>
		<br>
		<br>
		<label for="usuari-contrasenya">
		Contrasenya:</label><input type="text" name="usuari-contrasenya"/>
		<br>
		<input type="submit" value="Submit">
	</form:form>
	<a href="registre">Donar d'alta nou tripulant</a>
</body>
</html>