<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<p>donarAltaPersonal.jsp</p>
	<form:form action="procesarAltaPersonal" modelAttribute="personal">
		<p style="color: red;">${error}</p>
		DNI usuari: 
		<input type="text" name="dniPersonal">
		<br>
		<br>
		Nom usuari:
		<input type="text" name="nomPersonal">
		<br>
		<br>
		Contrasenya:
		<input type="text" name="contrasenyaPersonal">
		<br>
		<input type="submit" value="Submit">
	</form:form>
	
</body>
</html>