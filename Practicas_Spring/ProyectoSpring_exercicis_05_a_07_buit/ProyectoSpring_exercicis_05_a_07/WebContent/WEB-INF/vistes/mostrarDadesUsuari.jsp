<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Dades usuari</title>
</head>
<body>
	<h1>Dades de l'usuari</h1>
	<p>Nom: ${personal.nom}</p>
	<p>Cognom: ${personal.cognom}</p>
	<p>DNI: ${personal.dni}</p>
	<p>Edat: ${personal.edat}</p>
	<p>Email: ${personal.email}</p>
	<p>Departament: ${personal.departament}</p>
</body>
</html>