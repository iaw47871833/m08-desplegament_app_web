<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="exercici_05_a_07.Departament"%>
<%@page import="java.util.ArrayList"%>

<%
	ArrayList<Departament> departaments = (ArrayList<Departament>) request.getAttribute("departaments");
%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Modificar dades usuari</title>
</head>
<body>
	<p>modificarDadesUsuari.jsp</p>
	<form:form action="mostrarDadesUsuari" modelAttribute="personal">
	DNI usuari: 
	<form:input path="dni" />
	<form:errors path="dni" style="color:red"></form:errors><br/>
	Nom usuari: 
	<form:input path="nom"/><br/>
	Cognom:
	<form:input path="cognom"/>
	<form:errors path="cognom" style="color:red"></form:errors><br/>
	Email:
	<form:input path="email"/>
	<form:errors path="email" style="color:red"></form:errors><br/>
	Edat:
	<form:input path="edat"/>
	<form:errors path="edat" style="color:red"></form:errors><br/>
	Contrasenya: 
	<form:input path="contrasenya"/>
	<form:errors path="contrasenya" style="color:red"></form:errors><br/>
	Departament: 
	
 	<%
 	for (Departament dept : departaments) {
 %>
 	<form:radiobutton path="departament" value="<%= dept.getId() %>" label="<%= dept.getNom() %>"/>
 	
 	<%-- <input type="radio" name="departament" value="<%= dept.getId() %>"/>
 	<label for="departament"><%=dept.getNom() %></label> --%>
			 <%
 	}
	%>
	<br>
	<input type="submit" value="Submit">
	</form:form>
</body>
</html>