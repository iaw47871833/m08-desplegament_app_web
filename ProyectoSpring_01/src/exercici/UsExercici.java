package exercici;

import org.springframework.context.support.*;

public class UsExercici {

	public static void main(String[] args) {
		// Creació del contexte
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext_Exercici.xml");
		
		System.out.println("-------- Exercici 1 ---------");
		// Petició de beans al contenidor Spring
		Ruta rutaSingleton_1 = contexte.getBean("rutaSingleton", Ruta.class);
		//Ruta rutaSingleton_2 = contexte.getBean("rutaSingleton", Ruta.class);
		
		System.out.println("Direcció memòria rutaSingleton_1: " + rutaSingleton_1);
		System.out.println("Direcció memòria rutaSingleton_2: " + rutaSingleton_1);
		System.out.println("---------");
		System.out.println("rutaSingleton_1.getId(): " + rutaSingleton_1.getId());
		System.out.println("rutaSingleton_1.getNom(): " + rutaSingleton_1.getNom());
		System.out.println("rutaSingleton_1.getWaypoint(): " + rutaSingleton_1.getWaypoint());
		System.out.println("rutaSingleton_1.getLlistaWaypoints(): " + rutaSingleton_1.getLlistaWaypoints());
		System.out.println("rutaSingleton_1.getWaypoint().getId(): " + rutaSingleton_1.getWaypoint().getId());
		System.out.println("rutaSingleton_1.getWaypoint().getNom(): " + rutaSingleton_1.getWaypoint().getNom());
	
		System.out.println();
		System.out.println();
		
		System.out.println("-------- Exercici 2 ---------");
		Ruta rutaPrototype_1 = contexte.getBean("rutaPrototype", Ruta.class);
		Ruta rutaPrototype_2 = contexte.getBean("rutaPrototype", Ruta.class);
		
		System.out.println("Direcció memòria rutaPrototype_1: " + rutaPrototype_1);
		System.out.println("Direcció memòria rutaPrototype_2: " + rutaPrototype_2);
		System.out.println("---------");
		System.out.println("rutaPrototype_1.getId(): " + rutaPrototype_1.getId());
		System.out.println("rutaPrototype_1.getNom(): " + rutaPrototype_1.getNom());
		System.out.println("rutaPrototype_1.getWaypoint(): " + rutaPrototype_1.getWaypoint());
		System.out.println("rutaPrototype_1.getLlistaWaypoints(): " + rutaPrototype_1.getLlistaWaypoints());
		System.out.println("rutaPrototype_1.getWaypoint().getId(): " + rutaPrototype_1.getWaypoint().getId());
		System.out.println("rutaPrototype_1.getWaypoint().getNom(): " + rutaPrototype_1.getWaypoint().getNom());
		System.out.println("---------");
		System.out.println("rutaPrototype_2.getId(): " + rutaPrototype_2.getId());
		System.out.println("rutaPrototype_2.getNom(): " + rutaPrototype_2.getNom());
		System.out.println("rutaPrototype_2.getWaypoint(): " + rutaPrototype_2.getWaypoint());
		System.out.println("rutaPrototype_2.getLlistaWaypoints(): " + rutaPrototype_2.getLlistaWaypoints());
		System.out.println("rutaPrototype_2.getWaypoint().getId(): " + rutaPrototype_2.getWaypoint().getId());
		System.out.println("rutaPrototype_2.getWaypoint().getNom(): " + rutaPrototype_2.getWaypoint().getNom());
		
		System.out.println();
		System.out.println("L'objecte waypoint de rutaPrototype_1 i rutaPrototype_2 és el mateix perquè té la" +
		" mateixa dir. de memòria ja que el bean que crea els objectes de tipus Waypoint és Singleton.");
		System.out.println("Això implica que 2 rutes diferents (rutaPrototype_1 i rutaPrototype_2) tenen com" +
		" atribut el mateix objecte de la classe Waypoint (el comparteixen)");
		// Tanquem el contexte
		contexte.close();
	}

}
