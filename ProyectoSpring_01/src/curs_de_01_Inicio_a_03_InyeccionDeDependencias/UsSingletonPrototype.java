package curs_de_01_Inicio_a_03_InyeccionDeDependencias;

import org.springframework.context.support.*;

public class UsSingletonPrototype {

	public static void main(String[] args) {
		// Creem el contexte carregant el fitxer xml de configuració
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext_SingletonPrototype.xml");
		
		// Petició de beans al contenidor Spring
		Capita capita_1 = contexte.getBean("tripulantCapita", Capita.class);
		Capita capita_2 = contexte.getBean("tripulantCapita", Capita.class);
		
		// Com que Spring treballa per defecte amb el patró Singleton, capita_1 i capita_2 estaran apuntant al mateix
		// objecte. Imprimim els objectes capita_1 i capita_2 i veurem que la direcció de memòria és la mateixa i 
		// per tant són el mateix objecte.
		System.out.println("Patró SINGLETON:");
		System.out.println("Dir. de memòria de capita_1: " + capita_1);
		System.out.println("Dir. de memòria de capita_2: " + capita_2);
	
		if (capita_1 == capita_2) {
			System.out.println("capita_1 i capita_2 SI són el mateix objecte, per tant, apunten al mateix objecte.");
		} else {
			System.out.println("capita_1 i capita_2 NO són el mateix objecte, per tant, no apunten al mateix objecte.");
		}
		System.out.println();
		
		Electronic electronic_1 = contexte.getBean("tripulantElectronic", Electronic.class);
		Electronic electronic_2 = contexte.getBean("tripulantElectronic", Electronic.class);
		
		// En el xml hem configurat al bean "tripulantElectronic" amb scope=prototype de manera que electronic_1 i electronic_2 seran
		// 2 objectes diferents i per tant estaran apuntant a diferents objectes. Imprimim els objectes electronic_1 i electronic_2 i
		// veurem que la direcció de memòria és diferent i per tant són diferents objectes.
		System.out.println("Patró PROTOTYPE:");
		System.out.println("Dir. de memòria de electronic_1: " + electronic_1);
		System.out.println("Dir. de memòria de electronic_2: " + electronic_2);
		
		if (electronic_1 == electronic_2) {
			System.out.println("capita_1 i capita_2 SI són el mateix objecte, per tant, apunten al mateix objecte.");
		} else {
			System.out.println("capita_1 i capita_2 NO són el mateix objecte, per tant, no apunten al mateix objecte.");
		}
		System.out.println();
	
		// Tanquem el contexte quan ja no volem fer servir res més del seu contingut
		contexte.close();
		
		System.out.println("ABANS DE CARREGAR EL CONTEXT 2.");
		
		// Accedim al fitxer de configuracio de init i destroy
		ClassPathXmlApplicationContext contexte2 = new ClassPathXmlApplicationContext("applicationContext_InitDestroy.xml");
		
		System.out.println("DESPRÉS DE CARREGAR EL CONTEXT 2 I ABANS DE CARREGAR EL BEAN.");
	
		// Petició de beans al contenidor Spring
		Navegant navegant = contexte2.getBean("tripulantNavegant", Navegant.class);
		
		System.out.println("DESPRÉS DE CARREGAR EL BEAN.");
		
		System.out.println(navegant.agafarInforme());
		
		System.out.println("DESPRÉS D'EXECUTAR navegant.agafarInforme().");
		
		System.out.println(navegant.agafarTarees());
		
		System.out.println("DESPRÉS D'EXECUTAR navegant.agafarTarees().");
		
		// Tanquem el contexte
		contexte2.close();
		
		System.out.println("DESPRÉS DE TANCAR EL CONTEXTE 2");
	}

}
