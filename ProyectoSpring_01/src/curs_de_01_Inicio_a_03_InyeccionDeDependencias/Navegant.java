package curs_de_01_Inicio_a_03_InyeccionDeDependencias;

public class Navegant implements Tripulants {
	// Atributs
	private InformeInterface InformeNou; // Definim l'objecte que volem injectar (peró fent servir l'interface).

	/**
	 * @param informeNou the informeNou to set
	 */
	public void setInformeNou(InformeInterface informeNou) {
		this.InformeNou = informeNou;
	}

	@Override
	public String agafarTarees() {
		// TODO Auto-generated method stub
		return "Sóc un dels navegants.";
	}

	@Override
	public String agafarInforme() {
		// TODO Auto-generated method stub
		return "Informe del navegant. " + InformeNou.getInforme();
	}
	
	// Mètode init(): executar tarees abans de que el bean estigui disponible.
	public void metodeInit() {
		System.out.println("Acabem de llançar el mètodi init() de la classe Navegant.class just abans de que el bean estigui llest.");
	}
	
	// Mètode destroy()
	public void metodeDestroy() {
		System.out.println("Acabem de llançar el mètode destroy() de la classe Navegant.java just després de que el bean hagi estat destruit (hagi mort).");
	}

}
