package curs_de_01_Inicio_a_03_InyeccionDeDependencias;

import org.springframework.context.support.*;

public class UsTripulants {

	public static void main(String[] args) {
		// Creem el contexte carregant el fitxer xml de configuració
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext.xml");

		// Creem un objecte fent servir el bean (motor d'objectes de Spring)
		// d'applicationContext.xml (això és el Inversion of Control, IoC).
		// El 1r paràmetre és el ID del bean i el 2n és el tipus (la classe) que tindrà
		// l'objecte crear (una interface en aquest cas).
		// Amb:
		// Tripulants capita = contexte.getBean("tripulantCapita", Tripulants.class);
		// Creem un objecte "capita" de tipus Tripulants.java amb la classe
		// Tripulants.class (que és una interface).

		Tripulants capita = contexte.getBean("tripulantCapita", Tripulants.class);
		System.out.println(capita.agafarTarees());

		Tripulants maquinista = contexte.getBean("tripulantMaquinista", Tripulants.class);
		System.out.println(maquinista.agafarTarees());

		Tripulants electronic = contexte.getBean("tripulantElectronic", Tripulants.class);
		System.out.println(electronic.agafarTarees());
		
		// Creació d'un altre electronic perquè l'objecte Tripulant no conté
		// els métodes per retornar email i nom del departament
		Electronic electronic_2 = contexte.getBean("tripulantElectronic", Electronic.class);

		// Creació d'un altre maquinista
		Maquinista maquinista_2 = contexte.getBean("tripulantMaquinista", Maquinista.class);
		
		// Mostrem l'informe del capità
		System.out.println(capita.agafarInforme());
		
		// Mostrem l'informe del electronic
		System.out.println(electronic.agafarInforme());
		
		// Mostrem email i nom departament del electronic
		System.out.println("Email electronic: " + electronic_2.getEmail());
		System.out.println("Departament electronic: " + electronic_2.getNomDepartament());
		
		// Mostrem email i nom departament del maquinista
		System.out.println("Email maquinista: " + maquinista_2.getEmail());
		System.out.println("Departament maquinista: " + maquinista_2.getNomDepartament());
		
		// Tanquem el contexte (el fitxer xml) quan ja no volem fer servir res més del
		// seu contingut.
		contexte.close();
	}

}
