package curs_de_01_Inicio_a_03_InyeccionDeDependencias;

public class Maquinista implements Tripulants {
	
	// Email del maquinista
	private String email;
	// Nom del departament del maquinista
	private String nomDepartament;

	@Override
	public String agafarTarees() {
		return "Aquestes són les tarees del maquinista";
	}

	@Override
	public String agafarInforme() {
		return "Informe del capità";
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the nomDepartament
	 */
	public String getNomDepartament() {
		return nomDepartament;
	}

	/**
	 * @param nomDepartament the nomDepartament to set
	 */
	public void setNomDepartament(String nomDepartament) {
		this.nomDepartament = nomDepartament;
	}

}
