package curs_de_01_Inicio_a_03_InyeccionDeDependencias;

public class Capita implements Tripulants {
	
	// Declarem l'objecte que volem infectar (fem servir l'interface)
	private InformeInterface informeNou;
	
	public Capita(InformeInterface informeNou) {
		this.informeNou = informeNou;
	}
	
	@Override
	public String agafarTarees() {
		return "Sóc el capità de la nau Leonov";
	}

	@Override
	public String agafarInforme() {
		return "Informe del capità. " + informeNou.getInforme() ;
	}

}
