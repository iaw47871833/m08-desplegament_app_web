package curs_de_01_Inicio_a_03_InyeccionDeDependencias;

public class Electronic implements Tripulants {
	
	// Declarem l'objecte que volem injectar (però fent servir l'interface)
	private InformeInterface InformeNou;
	// Exemple d'inyecció per camps
	// Email del electronic
	private String email;
	// Nom del departament del electronic
	private String nomDepartament;
	
	@Override
	public String agafarTarees() {
		return "Sóc un dels electrònics.";
	}

	@Override
	public String agafarInforme() {
		return "Informe del tècnic en electrònica. " + InformeNou.getInforme();
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the nomDepartament
	 */
	public String getNomDepartament() {
		return nomDepartament;
	}

	/**
	 * @param nomDepartament the nomDepartament to set
	 */
	public void setNomDepartament(String nomDepartament) {
		this.nomDepartament = nomDepartament;
	}

	/**
	 * @param informeNou the informeNou to set
	 */
	public void setInformeNou(InformeInterface InformeNou) {
		this.InformeNou = InformeNou;
	}
}
